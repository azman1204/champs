@extends('layouts.induk')


@section('content-utama')
<div class="content-header">
<div class="container-fluid">

<table class="table">

    <tr align="center">
        <th> <h1 class="m-0">SISTEM PEMANTAUAN DAN SEMAKAN FAIL TUNTUTAN PEMBEKAL (MOSPAC) </h1> </th>
    </tr>

</table>

<div class="row mb-2">
<div class="col-sm-6">
<h1 class="m-0"></h1>
</div><!-- /.col -->
<div class="col-sm-6">
<ol class="breadcrumb float-sm-right">
<li class="breadcrumb-item"><a href="#">Home</a></li>
<li class="breadcrumb-item active">Dashboard
    
</li>
</ol>
</div><!-- /.col -->
</div><!-- /.row -->
</div><!-- /.container-fluid -->
</div>
<!-- /.content-header -->

<!-- Main content -->
<section class="content">
    <div class="container-fluid">
        <!-- Small boxes (Stat box) -->
        <div class="row">

            <div class="col-lg-4 col-md-4">
            <!-- small box -->
                <div class="small-box bg-info">
                    <div class="inner">
                        <h3>XX.XX%</h3>

                        <p>KEMAJUAN TUNTUTAN</p>
                    </div>
                    <div class="icon">
                        <i class="ion ion-stats-bars"></i>
                    </div>
                    <a href="#" class="small-box-footer">Lagi info <i class="fas fa-arrow-circle-right"></i></a>
                </div>
            </div>
            <!-- ./col -->

            <div class="col-lg-4 col-md-4">
            <!-- small box -->
                <div class="small-box bg-success">
                    <div class="inner">
                        <h3>XX,XXX,XXX.XX</h3>

                        <p>NILAI KONTRAK</p>
                    </div>
                <div class="icon">
                    <i class="ion ion-stats-bars"></i>
                </div>
                <a href="#" class="small-box-footer">Lagi info <i class="fas fa-arrow-circle-right"></i></a>
                </div>
            </div>
            <!-- ./col -->
            <div class="col-lg-4 col-md-4">
            <!-- small box -->
                <div class="small-box bg-warning">
                <div class="inner">
                <h3>X,XXX,XXX.XX</h3>

                <p>STATUS TUNTUTAN</p>
                </div>
                <div class="icon">
                <i class="ion ion-stats-bars"></i>
                </div>
                <a href="#" class="small-box-footer">Lagi info <i class="fas fa-arrow-circle-right"></i></a>
                </div>
            </div>
        <!-- content -->
        
                <table class="table table-bordered">

                    <thead>
                        <tr>
                            <th>Sistem aplikasi ini dibangunkan oleh Bahagian Pengurusan Maklumat untuk kegunaan Bahagian Padi dan Beras, bagi memudahkan pemantauan proses semakan dan bayaran tuntutan pembekal terutamanya merekod data tuntutan dan proses bayaran tuntutan kepada pembekal.
                            LPP merupakan salah satu Agensi yang dilantik oleh MAFI sebagai Agensi Pembayar bagi Bantuan Khas Kerajaan kepada Industri Padi dan Beras (BKKIPB).
                            </th>
                            
                        </tr>
                    </thead>

                    
                    </tbody>

                </table>
        </div>
        <!-- /.row -->
    <!-- Main row -->
    <div class="row">

    
    <!-- right col (We are only adding the ID to make the widgets sortable)-->
    <!-- right col -->
    </div>
    <!-- /.row (main row) -->
    </div><!-- /.container-fluid -->
</section>
@endsection
