@extends('layouts.induk')

@section('content-utama')

<div class="content-header">
    <div class="container-fluid">
        <div class="row mb-2">
        <div class="col-sm-6">
            <h1 class="m-0">Senarai Pengguna</h1>
        </div><!-- /.col -->
        <div class="col-sm-6">
            <ol class="breadcrumb float-sm-right">
            <li class="breadcrumb-item"><a href="/dashboard">Dashboard</a></li>
            <li class="breadcrumb-item active">Senarai Pengguna</li>
            </ol>
        </div><!-- /.col -->
        </div><!-- /.row -->
    </div><!-- /.container-fluid -->
</div>
<!-- /.content-header -->

<section class="content">
    <div class="container-fluid">
        <div class="card">
            <div class="card-body">
                <form method="post" action="/pengguna" class="mb-2">
                    @csrf
                    <div class="row">
                        <div class="col-md-3">
                            <label>Nama</label>
                            <input type="text" class="form-control" name="nama">
                        </div>

                        <div class="col-md-3">
                            <label>No KP</label>
                            <input type="text" class="form-control" name="nokp">
                        </div>

                        <div class="col-md-1">
                            <div>&nbsp;</div>
                            <input type="submit" class="btn btn-primary" value="Cari">
                        </div>
                    </div>
                </form>

                <table class="table table-bordered">

                    <thead>
                    <tr align="center">
                            <th>BIL.</th>
                            <th>NAMA PENGGUNA</th>
                            <th>NO. KAD PENGENALAN</th>
                            <th>EMAIL</th>
                            <th>NO. TELEFON</th>
                            <th>STATUS PENGGUNA</th>
                            <th>PERANAN</th>
                            <th>TINDAKAN</th>
                        </tr>
                    </thead>
                    <tbody>
                        @php $no = $senaraiPengguna->firstItem() @endphp
                        @forelse ($senaraiPengguna as $user)
                        <tr>
                            <td>{{ $no++ }}</td>
                            <td>{{ $user->nama }}</td>
                            <td>{{ $user->no_kp }}</td>
                            <td>{{ $user->email }}</td>
                            <td>{{ $user->telefon }}</td>
                            <td>{{ $user->status }}</td>
                            <td>
                                @foreach($user->getRoleNames() as $role)
                                    {{ $role }} <br>
                                @endforeach
                            </td>
                            <td align="center">
                               <!-- <a href="/pengguna/{{ $user->id }}/detail" class="btn btn-info bg-success">DETAIL</a> -->
                                <a href="/pengguna/{{ $user->id }}/edit" class="btn btn-info">KEMASKINI</a>
                                <a href="/pengguna/{{$user->id}}" class="btn btn-danger" onclick="
                                    var result = confirm('Adakah anda pasti untuk hapuskan pengguna ini?');

                                    if(result){
                                    event.preventDefault();
                                    document.getElementById('delete-form').submit();
                                    }">
                                    Delete
                                    </a>

                                <form method="POST" id="delete-form" action="{{route('users.destroy', [$user->id])}}">
                                {{csrf_field()}}
                                <input type="hidden" name="_method" value="DELETE">
                                </form>
                            </td>
                        </tr>
                        @empty
                        <tr>
                            <td colspan="6">TIADA REKOD</td>
                        </tr>
                        @endforelse
                    </tbody>
                </table>

                <div class="mt-2">{{ $senaraiPengguna->links() }}</div>
            </div>
        </div>
    </div>
</section>

@endsection
