@extends('layouts.induk')

@section('kod_css')
<link href="https://cdn.jsdelivr.net/npm/select2@4.1.0-rc.0/dist/css/select2.min.css" rel="stylesheet" />
@endsection

@push('script_extra')
<script src="https://cdn.jsdelivr.net/npm/select2@4.1.0-rc.0/dist/js/select2.min.js"></script>

<script>
    $("#select-peranan").select2({
        tags: true
    });
</script>
@endpush

@section('content-utama')

<div class="content-header">
    <div class="container-fluid">
        <div class="row mb-2">
        <div class="col-sm-6">
            <h1 class="m-0">Borang Pendaftaran Pengguna</h1>
        </div><!-- /.col -->
        <div class="col-sm-6">
            <ol class="breadcrumb float-sm-right">
            <li class="breadcrumb-item"><a href="/dashboard">Dashboard</a></li>
            <li class="breadcrumb-item active">Borang Pendaftaran Pengguna</li>
            </ol>
        </div><!-- /.col -->
        </div><!-- /.row -->
    </div><!-- /.container-fluid -->
</div>
<!-- /.content-header -->
<section class="content">

    <div class="container-fluid">

        <form method="POST" action="{{ route('users.store') }}">
            <div class="card">
                <div class="card-body">
                    @csrf

                    <div class="row">

                        <div class="col-md-6">

                            <div class="mb-3">
                                <label class="form-label">NO. KAD PENGENALAN</label>
                                <input type="text" class="form-control @error('no_kp') is-invalid @enderror" name="no_kp" value="{{ old('no_kp') }}" required>
                                <div class="form-text">Format: 808080080909</div>
                                @error('no_kp')
                                <div class="invalid-feedback">
                                    {{ $message }}.
                                </div>
                                @enderror
                            </div>

                        </div>
                        <div class="col-md-6">

                            <div class="mb-3">
                                <label class="form-label">NAMA PENUH PENGGUNA</label>
                                <input type="text" class="form-control @error('nama') is-invalid @enderror" name="nama" value="{{ old('nama') }}" required>
                                @error('nama')
                                <div class="invalid-feedback">
                                    {{ $message }}.
                                </div>
                                @enderror
                            </div>

                        </div>

                    </div>

                    <div class="row">

                        <div class="col-md-6">

                            <div class="mb-3">
                                <label class="form-label">EMAIL</label>
                                <input type="email" class="form-control @error('email') is-invalid @enderror" name="email" value="{{ old('email') }}" required>
                                @error('email')
                                <div class="invalid-feedback">
                                    {{ $message }}.
                                </div>
                                @enderror
                            </div>

                        </div>
                        <div class="col-md-6">

                            <div class="mb-3">
                                <label class="form-label">NO. TELEFON</label>
                                <input type="text" class="form-control @error('telefon') is-invalid @enderror" name="telefon" value="{{ old('telefon') }}">
                                @error('telefon')
                                <div class="invalid-feedback">
                                    {{ $message }}.
                                </div>
                                @enderror
                            </div>

                        </div>

                    </div>

                    <div class="row">
                        <div class="col-md-6">

                            <div class="mb-3">
                                <label class="form-label">KATALALUAN</label>
                                <input type="password" class="form-control @error('password') is-invalid @enderror" name="password">
                                @error('password')
                                <div class="invalid-feedback">
                                    {{ $message }}.
                                </div>
                                @enderror
                            </div>

                        </div>
                        <div class="col-md-6">

                            <div class="mb-3">
                                <label class="form-label">KATALALUAN (PENGESAHAN)</label>
                                <input type="password" class="form-control @error('password') is-invalid @enderror" name="password_confirmation">
                                @error('password')
                                <div class="invalid-feedback">
                                    {{ $message }}.
                                </div>
                                @enderror
                            </div>

                        </div>

                    </div>

                    <div class="row">
                        <div class="col-md-6">

                            <div class="mb-3">
                                <label class="form-label">PERANAN</label>

                                <select name="peranan[]" class="form-control" multiple="multiple" id="select-peranan">
                                    @foreach ($senaraiPeranan as $peranan)
                                        <option value="{{ $peranan->id }}" {{ old('peranan') == $peranan->id ? 'selected="selected"' : NULL }}>{{ $peranan->name }}</option>
                                    @endforeach
                                </select>

                            </div>

                        </div>
                        <div class="col-md-6">

                            <div class="mb-3">
                                <label class="form-label">JAWATAN</label>

                                <select name="jawatan_id" class="form-control">
                                    @foreach ($senaraiJawatan as $jawatan)
                                        <option value="{{ $jawatan->id }}" {{ old('jawatan_id') == $jawatan->id ? 'selected="selected"' : NULL }}>{{ $jawatan->nama }}</option>
                                    @endforeach
                                </select>

                            </div>

                        </div>

                    </div>

                    <div class="row">
                        <div class="col-md-6">

                            <div class="mb-3">
                                <label class="form-label">BAHAGIAN</label>

                                <select name="bahagian_id" class="form-control">
                                    @foreach ($senaraiBahagian as $bahagian)
                                        <option value="{{ $bahagian->id }}" {{ old('bahagian_id') == $bahagian->id ? 'selected="selected"' : NULL }}>{{ $bahagian->nama }}</option>
                                    @endforeach
                                </select>

                            </div>

                        </div>
                        <div class="col-md-6">

                            <div class="mb-3">
                                <label class="form-label">STATUS PENGGUNA</label>

                                <div class="form-check">
                                    <input class="form-check-input" type="radio" name="status" value="AKTIF" {{ old('status') == 1 ? 'checked' : NULL }}>
                                    <label class="form-check-label">
                                        AKTIF
                                    </label>
                                </div>
                                <div class="form-check">
                                    <input class="form-check-input" type="radio" name="status" value="TIDAK AKTIF" {{ old('status') == 0 ? 'checked' : NULL }}>
                                    <label class="form-check-label">
                                        TIDAK AKTIF
                                    </label>
                                </div>

                            </div>

                        </div>

                    </div>

                </div>
                <div class="card-footer">
                    <button type="submit" class="btn btn-primary">DAFTAR</button>
                </div>
            </div>
        </form>

    </div>

</section>

@endsection