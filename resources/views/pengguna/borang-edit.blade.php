@extends('layouts.induk')

@section('content-utama')

<div class="content-header">
    <div class="container-fluid">
        <div class="row mb-2">
        <div class="col-sm-6">
            <h1 class="m-0">Borang Kemaskini Pengguna</h1>
        </div><!-- /.col -->
        <div class="col-sm-6">
            <ol class="breadcrumb float-sm-right">
            <li class="breadcrumb-item"><a href="/dashboard">Dashboard</a></li>
            <li class="breadcrumb-item active">Borang Kemaskini Pengguna</li>
            </ol>
        </div><!-- /.col -->
        </div><!-- /.row -->
    </div><!-- /.container-fluid -->
</div>
<!-- /.content-header -->
<section class="content">

    <div class="container-fluid">

        <form method="POST" action="{{ route('users.update', $user->id) }}">
            @csrf
            @method('GET')
            <div class="card">
                <div class="card-body">
                        <div class="row">
                            <div class="col-md-6">
                                <div class="mb-3">
                                    <label class="form-label">NO. KAD PENGENALAN</label>
                                    <input type="text" class="form-control @error('no_kp') is-invalid @enderror" name="no_kp" value="{{ $user->no_kp }}" required>
                                    <div class="form-text">Format: 808080-08-0909</div>
                                    @error('no_kp')
                                    <div class="invalid-feedback">
                                        {{ $message }}.
                                    </div>
                                    @enderror
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="mb-3">
                                    <label class="form-label">NAMA PENUH PENGGUNA</label>
                                    <input type="text" class="form-control @error('nama') is-invalid @enderror" name="nama" value="{{ $user->nama }}" required>
                                    @error('nama')
                                    <div class="invalid-feedback">
                                        {{ $message }}.
                                    </div>
                                    @enderror
                                </div>
                            </div>
                        </div>

                        <div class="row">
                            <div class="col-md-6">
                                <div class="mb-3">
                                    <label class="form-label">EMAIL</label>
                                    <input type="email" class="form-control" name="email" value="{{ $user->email }}" required>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="mb-3">
                                    <label class="form-label">NO. TELEFON</label>
                                    <input type="text" class="form-control" name="telefon" value="{{ $user->telefon }}">
                                </div>
                            </div>
                        </div>

                        <div class="row">
                            <div class="col-md-6">
                                <div class="mb-3">
                                    <label class="form-label">KATALALUAN</label>
                                    <input type="password" class="form-control" name="password">
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="mb-3">
                                    <label class="form-label">KATALALUAN (PENGESAHAN)</label>
                                    <input type="password" class="form-control" name="password_confirmation">
                                </div>
                            </div>
                        </div>

                        <div class="row">
                            <div class="col-md-6">
                                <div class="mb-3">
                                    <label class="form-label">PERANAN</label>
                                    <br>
                                    @foreach ($senaraiPeranan as $peranan)
                                        <input type="checkbox" value="{{ $peranan->id }}" name="role_id[]"
                                        {{ $roles->has($peranan->id) ? 'checked' : NULL }}>
                                        {{ $peranan->name }}
                                        <br>
                                    @endforeach
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="mb-3">
                                    <label class="form-label">JAWATAN</label>
                                    <select name="jawatan_id" class="form-control">
                                        @foreach ($senaraiJawatan as $jawatan)
                                            <option value="{{ $jawatan->id }}" {{ $user->jawatan_id == $jawatan->id ? 'selected="selected"' : NULL }}>{{ $jawatan->nama }}</option>
                                        @endforeach
                                    </select>
                                </div>
                            </div>
                        </div>

                        <div class="row">
                            <div class="col-md-6">
                                <div class="mb-3">
                                    <label class="form-label">BAHAGIAN</label>

                                    <select name="bahagian_id" class="form-control">
                                        @foreach ($senaraiBahagian as $bahagian)
                                            <option value="{{ $bahagian->id }}" {{ $user->bahagian_id == $bahagian->id ? 'selected="selected"' : NULL }}>{{ $bahagian->nama }}</option>
                                        @endforeach
                                    </select>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="mb-3">
                                    <label class="form-label">STATUS PENGGUNA</label>

                                    <div class="form-check">
                                        <input class="form-check-input" type="radio" name="status"
                                        value="AKTIF" {{ $user->status == 'AKTIF' ? 'checked' : NULL }}>
                                        <label class="form-check-label">
                                        AKTIF
                                        </label>
                                    </div>
                                    <div class="form-check">
                                        <input class="form-check-input" type="radio" name="status"
                                        value="TIDAK AKTIF" {{ $user->status == 'TIDAK AKTIF' ? 'checked' : NULL }}>
                                        <label class="form-check-label">
                                        TIDAK AKTIF
                                        </label>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div class="row">
                            <div class="col-md-6">
                                <div class="mb-3">
                                    <label class="form-label">CAWANGAN</label>
                                    {{ Form::select('cawangan_id', $cawangan, $user->cawangan_id, ['class' => 'form-control']) }}
                                </div>
                            </div>
                        </div>
                </div>
                <div class="card-footer">
                    <button type="submit" class="btn btn-primary">SIMPAN</button>
                </div>
            </div>
        </form>

    </div>

</section>

@endsection
