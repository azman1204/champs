@extends('layouts.induk')

@section('content-utama')

<div class="content-header">
    <div class="container-fluid">
        <div class="row mb-2">
        <div class="col-sm-6">
            <h1 class="m-0">Borang Kemaskini Program / Skim</h1>
        </div><!-- /.col -->
        <div class="col-sm-6">
            <ol class="breadcrumb float-sm-right">
            <li class="breadcrumb-item"><a href="/dashboard">Dashboard</a></li>
            <li class="breadcrumb-item active">Borang Kemaskini Program / Skim</li>
            </ol>
        </div><!-- /.col -->
        </div><!-- /.row -->
    </div><!-- /.container-fluid -->
</div>
<!-- /.content-header -->
<section class="content">

    <div class="container-fluid">

        <form method="POST" action="{{ route('skim.update', $skim->id) }}">
            @csrf
            @method('GET')
            <div class="card">
                <div class="card-body">

                        <div class="row">

                            <div class="col-md-6">

                                <div class="mb-3">
                                    <label class="form-label">NAMA PROGRAM / SKIM</label>
                                    <input type="text" class="form-control @error('nama') is-invalid @enderror" name="nama" value="{{ $skim->nama }}" value="{{ old('nama') }}">
                                    @error('nama')
                                    <div class="invalid-feedback">
                                        {{ $message }}.
                                    </div>
                                    @enderror
                                </div>

                            </div>
                            <div class="col-md-6">

                                <div class="mb-3">
                                <label class="form-label">VOT PROGRAM / SKIM</label>
                                    <input type="text" class="form-control @error('vot') is-invalid @enderror" name="vot" value="{{ $skim->vot }}" required>
                                    <div class="form-text">Format: 8-080-8080-90909</div>
                                    @error('vot')
                                    <div class="invalid-feedback">
                                        {{ $message }}.
                                    </div>
                                    @enderror
                                </div>

                            </div>

                        </div>

                        <div class="row">

                            <div class="col-md-6">

                                <div class="mb-3">
                                    <label class="form-label">TAHUN</label>
                                    <input type="number" class="form-control" name="tahun" value="{{ $skim->tahun }}" value="{{ old('tahun') }}"> 
                                </div>

                            </div>
                        
                            <div class="col-md-6">

                                <div class="mb-3">
                                    <label class="form-label">JUMLAH PERUNTUKAN (RM)</label>
                                    <input type="number" class="form-control @error('jumlah_peruntukan') is-invalid @enderror" name="jumlah_peruntukan" value="{{ $skim->jumlah_peruntukan }}" min="0.01" step="0.01" value="{{ old('jumlah_peruntukan') }}">
                                </div>

                            </div>

                            <div class="col-md-6">

                                <div class="mb-3">
                                    <label class="form-label">BAKI PERUNTUKAN (RM)</label>
                                    <input type="number" class="form-control @error('baki_peruntukan') is-invalid @enderror" name="baki_peruntukan" value="{{ $skim->baki_peruntukan }}" min="0.01" step="0.01" value="{{ old('baki_peruntukan') }}">
                                </div>

                            </div>

                            <div class="col-md-6">

                                <div class="mb-3">
                                    <label class="form-label">WIREMENT TAHUN SEMASA (RM)</label>
                                    <input type="number" class="form-control @error('wirement') is-invalid @enderror" name="wirement" value="{{ $skim->wirement }}" min="0.01" step="0.01" value="{{ old('wirement') }}">
                                </div>

                            </div>
                        
                        <div class="row">

                            <div class="col-md-4">

                                <div class="mb-3">
                                    <label class="form-label">PELARASAN (RM)</label>
                                    <input type="number" class="form-control @error('pelarasan') is-invalid @enderror" name="pelarasan" value="{{ $skim->pelarasan }}" min="0.01" step="0.01" value="{{ old('pelarasan') }}">
                                </div>

                            </div>

                            <div class="col-md-4">

                                <div class="mb-3">
                                    <label class="form-label">BELANJA TAHUN SEMASA (RM)</label>
                                    <input type="number" class="form-control @error('belanja') is-invalid @enderror" name="belanja" value="{{ $skim->belanja }}" min="0.01" step="0.01" value="{{ old('belanja') }}">
                                </div>

                            </div>

                            <div class="col-md-4">

                                <div class="mb-3">
                                    <label class="form-label">TARIKH KELULUSAN</label>
                                    <input type="date" class="form-control @error('tarikh_kelulusan') is-invalid @enderror" name="tarikh_kelulusan" value="{{ $skim->tarikh_kelulusan }}" value="{{ old('tarikh_kelulusan') }}">
                                </div>
                            </div>

                            <div class="col-md-4">

                                <div class="mb-3">
                                    <label class="form-label">RUJUKAN KELULUSAN</label>
                                    <input type="text" class="form-control @error('rujukan_kelulusan') is-invalid @enderror" name="rujukan_kelulusan" value="{{ $skim->rujukan_kelulusan }}" value="{{ old('rujukan_kelulusan') }}">
                                </div>

                            </div>

                            <div class="col-md-4">

                                <div class="mb-3">
                                    <label class="form-label">CATATAN</label>
                                    <input type="text" class="form-control @error('catatan') is-invalid @enderror" name="catatan" value="{{ $skim->catatan }}" value="{{ old('catatan') }}">

                                </div>
                            </div>
                        </div>
                    
                </div>
                
                <div class="card-footer">
                    <button type="submit" class="btn btn-primary">SIMPAN</button>
                </div>
            </div>
        </form>

    </div>

</section>

@endsection
