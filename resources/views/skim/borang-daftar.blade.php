@extends('layouts.induk')

@section('kod_css')
<link href="https://cdn.jsdelivr.net/npm/select2@4.1.0-rc.0/dist/css/select2.min.css" rel="stylesheet" />
@endsection

@push('script_extra')
<script src="https://cdn.jsdelivr.net/npm/select2@4.1.0-rc.0/dist/js/select2.min.js"></script>

<script>
    $("#select-peranan").select2({
        tags: true
    });
</script>
@endpush

@section('content-utama')

<div class="content-header">
    <div class="container-fluid">
        <div class="row mb-2">
        <div class="col-sm-6">
            <h1 class="m-0">Borang Pendaftaran Program / Skim</h1>
        </div><!-- /.col -->
        <div class="col-sm-6">
            <ol class="breadcrumb float-sm-right">
            <li class="breadcrumb-item"><a href="/dashboard">Dashboard</a></li>
            <li class="breadcrumb-item active">Borang Pendaftaran Program / Skim</li>
            </ol>
        </div><!-- /.col -->
        </div><!-- /.row -->
    </div><!-- /.container-fluid -->
</div>
<!-- /.content-header -->
<section class="content">

    <div class="container-fluid">

        <form method="POST" action="{{ route('skim.store') }}">
            <div class="card">
                <div class="card-body">
                    @csrf

                    <div class="row">

                        <div class="col-md-6">

                            <div class="mb-3">
                                <label class="form-label">NAMA PROGRAM / SKIM</label>
                                <input type="text" class="form-control @error('nama') is-invalid @enderror" name="nama" value="{{ old('nama') }}" required>
                                @error('nama')
                                <div class="invalid-feedback">
                                    {{ $message }}.
                                </div>
                                @enderror
                            </div>

                        </div>
                        <div class="col-md-6">

                            <div class="mb-3">
                                <label class="form-label">VOT PROGRAM / SKIM</label>
                                <input type="text" class="form-control @error('vot') is-invalid @enderror" name="vot" value="{{ old('vot') }}" required>
                                @error('vot')
                                <div class="invalid-feedback">
                                    {{ $message }}.
                                </div>
                                @enderror
                            </div>

                        </div>

                    </div>

                    <div class="row">

                        <div class="col-md-6">
                            <div class="mb-3">
                                <label class="form-label">TAHUN</label>
                                <input type="text" class="form-control @error('tahun') is-invalid @enderror" name="tahun" value="{{ old('tahun') }}" required>
                                @error('tahun')
                                <div class="invalid-feedback">
                                    {{ $message }}.
                                </div>
                                @enderror
                            </div>

                        </div>
                        <div class="col-md-6">

                            <div class="mb-3">
                            <label class="form-label">JUMLAH PERUNTUKAN (RM)</label>
                                <input type="number" class="form-control @error('jumlah_peruntukan') is-invalid @enderror"" name="jumlah_peruntukan" min="0.01" step="0.01">
                                @error('jumlah_peruntukan')
                                <div class="invalid-feedback">
                                     {{ $message }}.
                                </div>
                                @enderror
                            </div>

                        </div>
                        <div class="col-md-6">

                            <div class="mb-3">
                                <label class="form-label">BAKI PERUNTUKAN (RM)</label>
                                <input type="number" class="form-control @error('baki_peruntukan') is-invalid @enderror"" name="baki_peruntukan" min="0.01" step="0.01">
                                @error('baki_peruntukan')
                                <div class="invalid-feedback">
                                    {{ $message }}.
                                </div>
                                @enderror
                            </div>
                        </div>

                    <div class="row">
                        <div class="col-md-6">
                            
                            <div class="mb-3">
                            <label class="form-label">TARIKH KELULUSAN</label>
                                <input type="date" class="form-control @error('tarikh_kelulusan') is-invalid @enderror" name="tarikh_kelulusan">
                                @error('tarikh_kelulusan')
                                <div class="invalid-feedback">
                                        {{ $message }}.
                                </div>
                                @enderror
                            </div>
                        </div>

                        <div class="col-md-6">

                            <div class="mb-3">
                                <label class="form-label">RUJUKAN KELULUSAN</label>
                                <input type="text" class="form-control @error('rujukan_kelulusan') is-invalid @enderror" name="rujukan_kelulusan" value="{{ old('rujukan_kelulusan') }}">
                                @error('rujukan_kelulusan')
                                <div class="invalid-feedback">
                                    {{ $message }}.
                                </div>
                                @enderror
                            </div>
                        </div>
                    </div>
                        <div class="col-md-6">

                            <div class="mb-3">
                                <label class="form-label">CATATAN</label>
                                <input type="text" class="form-control @error('catatan') is-invalid @enderror" name="catatan" value="{{ old('catatan') }}">
                                @error('catatan')
                                <div class="invalid-feedback">
                                    {{ $message }}.
                                </div>
                                @enderror
                            </div>

                        </div>
                    </div>
            </div>
                
                <div class="card-footer">
                    <button type="submit" class="btn btn-primary">DAFTAR</button>
                </div>
            </div>
        </form>

    </div>

</section>

@endsection
