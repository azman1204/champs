@extends('layouts.induk')

@section('content-utama')

<div class="content-header">
    <div class="container-fluid">
        <div class="row mb-2">
        <div class="col-sm-6">
            <h1 class="m-0">Detail Tuntutan</h1>
        </div><!-- /.col -->
        <div class="col-sm-6">
            <ol class="breadcrumb float-sm-right">
            <li class="breadcrumb-item"><a href="{{ route('dashboard') }}">Dashboard</a></li>
            <li class="breadcrumb-item active">Detail Tuntutan</li>
            </ol>
        </div><!-- /.col -->
        </div><!-- /.row -->
    </div><!-- /.container-fluid -->
</div>
<!-- /.content-header -->

<section class="content">
    <div class="container-fluid">
        <div class="card">
            <div class="card-body">

                <form method="POST" action="{{ route('tuntutan.store') }}">
                    @csrf

                    <div class="card">
                        <div class="card-body">

                            <div class="row">

                                <div class="col-md-6">

                                    <div class="mb-3">
                                        <label class="form-label">NO. RUJUKAN FAIL TUNTUTAN</label>
                                        <input type="text" class="form-control" name="no_fail" value="{{ $tuntutan->no_fail }}" disabled>
                                    </div>

                                </div>
                                <div class="col-md-6">

                                    <div class="mb-3">
                                        <label class="form-label">NAMA PEMBEKAL</label>
                                        <input type="text" class="form-control" value="{{ $tuntutan->pembekal->nama }}" disabled>
                                    </div>

                                </div>

                            </div>

                            <div class="row">


                                <div class="col-md-6">

                                    <div class="mb-3">
                                        <label class="form-label">NAMA PROGRAM/SKIM</label>
                                        <input type="text" class="form-control" value="{{ $tuntutan->skim->nama }}" disabled>
                                    </div>

                                </div>

                                <div class="col-md-6">

                                    <div class="mb-3">
                                        <label class="form-label">TAHUN</label>
                                        <input type="number" class="form-control" disabled value="{{ $tuntutan->tahun }}">
                                    </div>

                                </div>

                            </div>

                            <div class="row">

                                <div class="col-md-6">

                                    <div class="mb-3">
                                        <label class="form-label">TARIKH TUNTUTAN</label>
                                        <input type="date" class="form-control" name="tarikh_tuntutan" value="{{ $tuntutan->tarikh_tuntutan }}" disabled>
                                    </div>

                                </div>

                                <div class="col-md-6">

                                    <div class="mb-3">
                                        <label class="form-label">JUMLAH TUNTUTAN (RM)</label>
                                        <input type="number" class="form-control" name="jumlah_tuntutan" value="{{ $tuntutan->jumlah_tuntutan }}" disabled>
                                    </div>

                                </div>

                            </div>

                            <div class="row">

                                <div class="col-md-4">

                                    <div class="mb-3">
                                        <label class="form-label">JUMLAH PERUNTUKAN (RM)</label>
                                        <input type="number" class="form-control" value="{{ $tuntutan->jumlah_peruntukan }}" disabled>
                                    </div>

                                </div>

                                <div class="col-md-4">

                                    <div class="mb-3">
                                        <label class="form-label">BAKI PERUNTUKAN (RM)</label>
                                        <input type="number" class="form-control " value="{{ $tuntutan->baki_peruntukan }}" disabled>
                                        @error('baki_peruntukan')
                                        <div class="invalid-feedback">
                                            {{ $message }}.
                                        </div>
                                        @enderror
                                    </div>

                                </div>

                                <div class="col-md-4">

                                    <div class="mb-3">
                                        <label class="form-label">KUOTA (RM)</label>
                                        <input type="number" class="form-control" value="{{ $tuntutan->kuota }}" disabled>
                                        @error('jumlah_tuntutan')
                                        <div class="invalid-feedback">
                                            {{ $message }}.
                                        </div>
                                        @enderror
                                    </div>

                                </div>

                                <!-- Status -->
                                <hr>

                                    <table class="table table-bordered">

                                        <thead>
                                            <tr>
                                                <th>STATUS</th>
                                                <th>TINDAKAN</th>
                                            </tr>
                                        </thead>

                                    <tbody>
                                        @forelse ($tuntutan->status as $status)
                                        <tr>
                                            <td>{{ $status->status }}</td>
                                        </tr>
                                        @empty
                                        <tr>
                                            <td>TIADA REKOD</td>
                                        </tr>
                                         @endforelse
                                    </tbody>

                                    </table>
                                </hr>   
                            </div>

                            </div>
                        </div>
                        <div class="card-footer">
                            
                            <a href="{{ route('tuntutan.edit', $tuntutan->id) }}" class="btn btn-primary">KEMASKINI</button>

                            <a href="{{ route('tuntutan.print', $tuntutan->id) }}" class="btn btn-warning">CETAK</a>

                            {{-- Paparkan butang cetak jika ada status tertentu
                            @if ($tuntutan->status == Tuntutan::STATUS_BAYARAN_SELESAI)
                            <a href="{{ route('tuntutan.print', $tuntutan->id) }}" class="btn btn-warning">CETAK</a>
                            @endif
                             --}}
                        </div>
                    </div>
                </form>

            </div>
        </div>

    </div>
</section>

@endsection
