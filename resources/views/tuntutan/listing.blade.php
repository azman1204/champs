@extends('layouts.induk')
@section('content-utama')
<table class="table table-bordered">
    <thead>
        <tr>
            <th>Bil</th>
            <th>No rujukan Fail</th>
            <th>Jumlah Tuntutan</th>
            <th>Tarikh Tuntutan</th>
            <th>Jumlah Peruntukan</th>
            <th>Status</th>
            <th>Tindakan</th>
        </tr>
    </thead>
    <tbody>
        @foreach($rows as $row)
        <tr>
            <td></td>
            <td>{{ $row->no_rujukan_fail }}</td>
            <td>{{ $row->jum_tuntutan }}</td>
            <td>{{ $row->tkh_tuntutan }}</td>
            <td>{{ $row->jum_kontrak }}</td>
            <td>{{ $status[$row->status] }}</td>
            <td></td>
        </tr>
        @endforeach
    </tbody>
</table>
@endsection
