<div style="width:550px;">
    <h1>{{ $tuntutan->no_fail }}</h1>
    <p>
      After all the battles we faught together, after all the difficult times we saw together, after all the good and bad moments we've been through, I think it's time I let you know how I feel about you.
    </p>

    <h2>Pembekal: {{ $tuntutan->pembekal->nama }}</h2>
    <p>
      You complete my darkness with your light. I love:
    </p>
    <ul>
      <li>the way you see good in the worse</li>
      <li>the way you handle emotionally difficult situations</li>
      <li>the way you look at Justice</li>
    </ul>
    <p>
      I have learned a lot from you. You have occupied a special place in my heart over the time.
    </p>
</div>
