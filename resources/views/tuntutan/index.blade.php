@extends('layouts.induk')

@section('kod_css')
<link href="https://cdn.datatables.net/1.13.1/css/dataTables.bootstrap5.min.css">
<link href="https://cdn.datatables.net/1.13.1/css/dataTables.bootstrap5.min.css">
@endsection

@push('script_extra')
<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.6.1/jquery.min.js" integrity="sha512-aVKKRRi/Q/YV+4mjoKBsE4x3H+BkegoM/em46NNlCqNTmUYADjBbeNefNxYV7giUp0VxICtqdrbqU7iVaeZNXA==" crossorigin="anonymous" referrerpolicy="no-referrer"></script>
<script src="//cdn.datatables.net/1.13.1/js/jquery.dataTables.min.js"></script>
<script src="https://cdn.datatables.net/1.13.1/js/dataTables.bootstrap5.min.js"></script>

<script>
    $(function() {
        $('#datatables').DataTable({
            processing: true,
            serverSide: true,
            ajax: {
                type: 'POST',
                    'headers': {
                        'X-CSRF-TOKEN': '{{ csrf_token() }}'
                    },
                url: '{!! route('tuntutan.datatables') !!}'
            },
            columns: [
                { data: 'id', name: 'id' },
                { data: 'no_fail', name: 'no_fail' },
                { data: 'skim.nama', name: 'skim.nama' },
                { data: 'pembekal.nama', name: 'pembekal.nama' },
                { data: 'jumlah_tuntutan', name: 'jumlah_tuntutan' },
                { data: 'jumlah_peruntukan', name: 'jumlah_peruntukan' },
                { data: 'baki_peruntukan', name: 'baki_peruntukan' },
                { data: 'tarikh_tuntutan', name: 'tarikh_tuntutan' },
                { data: 'tindakan', name: 'tindakan', orderable: false, searchable: false }
            ]
        });
    });
</script>

@endpush

@section('content-utama')

<div class="content-header">
    <div class="container-fluid">
        <div class="row mb-2">
        <div class="col-sm-6">
            <h1 class="m-0">Senarai Tuntutan</h1>
        </div><!-- /.col -->
        <div class="col-sm-6">
            <ol class="breadcrumb float-sm-right">
            <li class="breadcrumb-item"><a href="{{ route('dashboard') }}">Dashboard</a></li>
            <li class="breadcrumb-item active">Senarai Tuntutan</li>
            </ol>
        </div><!-- /.col -->
        </div><!-- /.row -->
    </div><!-- /.container-fluid -->
</div>
<!-- /.content-header -->

<section class="content">
    <div class="container-fluid">
        <div class="card">
            <div class="card-body">

                <table class="table table-bordered" id="datatables">

                    <thead>
                    <tr align="center">
                            <th>BIL.</th>
                            <th>NO.RUJUKAN FAIL</th>
                            <th>NAMA SKIM/PROGRAM</th>
                            <th>NAMA PEMBEKAL</th>
                            <th>JUMLAH TUNTUTAN (RM)</th>
                            <th>JUMLAH PERUNTUKAN (RM)</th>
                            <th>BAKI PERUNTUKAN (RM)</th>
                            <th>TARIKH TUNTUTAN</th>
                            <th>TINDAKAN</th>
                        </tr>
                    </thead>


                </table>

            </div>
        </div>

    </div>
</section>

@endsection
