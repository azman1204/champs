@extends('layouts.induk')
@section('content-utama')

<link href="/chosen_v1.8.7/chosen.css" type="text/css" rel="stylesheet">
<div class="content-header">
    <div class="container-fluid">
        <div class="row mb-2">
        <div class="col-sm-6">
            <h1 class="m-0">Daftar Tuntutan</h1>
        </div><!-- /.col -->
        <div class="col-sm-6">
            <ol class="breadcrumb float-sm-right">
            <li class="breadcrumb-item"><a href="{{ route('dashboard') }}">Dashboard</a></li>
            <li class="breadcrumb-item active">Daftar Tuntutan</li>
            </ol>
        </div><!-- /.col -->
        </div><!-- /.row -->
    </div><!-- /.container-fluid -->
</div>
<!-- /.content-header -->

<section class="content">
    <div class="container-fluid">
        <div class="card">
            <div class="card-body">

                <form method="POST" action="{{ route('tuntutan.store') }}">
                    @csrf
                    <div class="card">
                        <div class="card-body">
                            <div class="row">
                                <div class="col-md-6">
                                    <div class="mb-3">
                                        <label class="form-label">NO. RUJUKAN FAIL TUNTUTAN</label>
                                        <input type="text" class="form-control @error('no_fail') is-invalid @enderror" name="no_rujukan_fail" value="{{ old('no_fail') }}" required>
                                        @error('no_fail')
                                        <div class="invalid-feedback">
                                            {{ $message }}.
                                        </div>
                                        @enderror
                                    </div>
                                </div>
                            </div>

                            <div class="row">
                                <div class="col-md-6">
                                    <div class="mb-3">
                                        <label class="form-label">NAMA PEMBEKAL</label>
                                        {{ Form::select('pp_id', $senaraiPembekal, '', ['id' => 'pembekal_id', 'class' => 'form-control']) }}
                                    </div>
                                </div>
                            </div>

                            <div class="row">
                                <div class="col-md-6">
                                    <div class="mb-3">
                                        <label class="form-label">TARIKH TUNTUTAN</label>
                                        <input type="date" class="form-control @error('tarikh_tuntutan') is-invalid @enderror" name="tkh_tuntutan">
                                        @error('tarikh_tuntutan')
                                        <div class="invalid-feedback">
                                            {{ $message }}.
                                        </div>
                                        @enderror
                                    </div>

                                </div>

                                <div class="col-md-6">

                                    <div class="mb-3">
                                        <label class="form-label">JUMLAH TUNTUTAN (RM)</label>
                                        <input type="number" class="form-control @error('jumlah_tuntutan') is-invalid @enderror" name="jum_tuntutan" min="0.01" step="0.01">
                                        @error('jumlah_tuntutan')
                                        <div class="invalid-feedback">
                                            {{ $message }}.
                                        </div>
                                        @enderror
                                    </div>

                                </div>

                            </div>

                            <div class="row">

                                <div class="col-md-12">

                                    <div class="mb-3">
                                        <label class="form-label">PANEL PENYEMAK</label>
                                        <select name="penyemak_id" class="form-control">
                                            @foreach ($senaraiPanel as $panel)
                                                <option value="{{ $panel->id }}" {{ old('panel') == $panel->id ? 'selected="selected"' : NULL }}>{{ $panel->nama }}</option>
                                            @endforeach
                                        </select>
                                        @error('user_id')
                                        <div class="invalid-feedback">
                                            {{ $message }}.
                                        </div>
                                        @enderror
                                    </div>

                                </div>

                            </div>
                        </div>
                        <div class="card-footer">
                            <button type="submit" class="btn btn-primary">DAFTAR</button>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
</section>
@endsection

@push('script_extra')
    <script src="/chosen_v1.8.7/chosen.jquery.js"></script>
    <script>
        $('#pembekal_id').chosen();
    </script>
@endpush
