@extends('layouts.induk')

@section('content-utama')

<div class="content-header">
    <div class="container-fluid">
        <div class="row mb-2">
        <div class="col-sm-6">
            <h1 class="m-0">Detail Tuntutan {{ $tuntutan->no_fail }}</h1>
        </div><!-- /.col -->
        <div class="col-sm-6">
            <ol class="breadcrumb float-sm-right">
            <li class="breadcrumb-item"><a href="{{ route('dashboard') }}">Dashboard</a></li>
            <li class="breadcrumb-item active">Detail Tuntutan</li>
            </ol>
        </div><!-- /.col -->
        </div><!-- /.row -->
    </div><!-- /.container-fluid -->
</div>
<!-- /.content-header -->

<section class="content">
    <div class="container-fluid">
        <div class="card">
            <div class="card-body">

                <table class="table table-bordered">

                    <thead>
                        <tr>
                            <th>PERKARA</th>
                            <th>BUTIRAN</th>
                        </tr>
                    </thead>

                    <tbody>
                        <tr>
                            <td>NO FAIL</td>
                            <td>{{ $tuntutan->no_fail }}</td>
                        </tr>
                        <tr>
                            <td>PEMBEKAL</td>
                            <td>{{ $tuntutan->pembekal->nama }}</td>
                        </tr>
                        <tr>
                            <td>SKIM/PROGRAM</td>
                            <td>{{ $tuntutan->skim->nama }}</td>
                        </tr>
                        <tr>
                            <td>TAHUN</td>
                            <td>{{ $tuntutan->tahun }}</td>
                        </tr>
                        <tr>
                            <td>TARIKH TUNTUTAN</td>
                            <td>{{ $tuntutan->tarikh_tuntutan }}</td>
                        </tr>
                        <tr>
                            <td>JUMLAH TUNTUTAN (RM)</td>
                            <td>{{ $tuntutan->jumlah_tuntutan }}</td>
                        </tr>
                        <tr>
                            <td>JUMLAH PERUNTUKAN (RM)</td>
                            <td>{{ $tuntutan->jumlah_peruntukan }}</td>
                        </tr>
                        <tr>
                            <td>BAKI PERUNTUKAN (RM)</td>
                            <td>{{ $tuntutan->baki_peruntukan }}</td>
                        </tr>
                        <tr>
                            <td>KUOTA (RM)</td>
                            <td>{{ $tuntutan->kuota }}</td>
                        </tr>
                    </tbody>

                </table>

                <hr>

                <table class="table table-bordered">

                    <thead>
                        <tr>
                            <th>STATUS</th>
                        </tr>
                    </thead>

                    <tbody>
                        @forelse ($tuntutan->status as $status)
                        <tr>
                            <td>{{ $status->status }}</td>
                        </tr>
                        @empty
                        <tr>
                            <td>TIADA REKOD</td>
                        </tr>
                        @endforelse
                    </tbody>

                </table>

            </div>
        </div>

    </div>
</section>

@endsection
