@extends('layouts.induk')
@section('content-utama')
<h1>PRESTASI TUNTUTAN BAYARAN DAN KUOTA</h1>
<form action="/laporan-tuntutan" method="post">
    @csrf
    <div class="row">
        <div class="col-md-4">
            <label>Tarikh Dari</label>
            <input type="date" name="tkh_dari">
        </div>
        <div class="col-md-4">
            <label>Tarikh Hingga</label>
            <input type="date" name="tkh_hingga">
        </div>
        <div class="col-md-1">
            <input type="submit" value="Cari" class="btn btn-primary">
        </div>
    </div>
</form>

<table class="table table-bordered">
    <thead>
        <tr>
            <th>Bil</th>
            <th>Pengeluar</th>
            <th>Kuota</th>
            <th>Jum Tuntutan (MT)</th>
            <th>Jum Tuntutan (RM)</th>
            <th>%</th>
            <th>Bayaran</th>
        </tr>
    </thead>
    <tbody>
        @foreach($rows as $row)
        <tr>
            <td>{{ $loop->iteration }}</td>
            <td>{{ $row->nama_pembekal }}</td>
            <td>{{ $row->kuota }}</td>
            <td>{{ $row->tm }}</td>
            <td>{{ $row->jum_tuntutan }}</td>
            <td>{{ $row->pct }}</td>
            <td>{{ $row->jum_bayar }}</td>
        </tr>
        @endforeach
    </tbody>
</table>
@endsection
