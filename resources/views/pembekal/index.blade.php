@extends('layouts.induk')

@section('content-utama')

<div class="content-header">
    <div class="container-fluid">
        <div class="row mb-2">
        <div class="col-sm-6">
            <h1 class="m-0">Senarai Pembekal</h1>
        </div><!-- /.col -->
        <div class="col-sm-6">
            <ol class="breadcrumb float-sm-right">
            <li class="breadcrumb-item"><a href="{{ route('dashboard') }}">Dashboard</a></li>
            <li class="breadcrumb-item active">Senarai Pembekal</li>
            </ol>
        </div><!-- /.col -->
        </div><!-- /.row -->
    </div><!-- /.container-fluid -->
</div>
<!-- /.content-header -->

<section class="content">
    <div class="container-fluid">
        <div class="card">
            <div class="card-body">

                <table class="table table-bordered">

                    <thead>
                    <tr align="center">
                            <th>BIL.</th>
                            <th>NAMA PEMBEKAL</th>
                            <th>KOD PEMBEKAL</th>
                            <th>ALAMAT PEMBEKAL</th>
                            <th>NO.TELEFON PEMBEKAL</th>
                            <th>NO.FAX PEMBEKAL</th>
                            <th>TAHUN</th>
                            <th>PEGAWAI BERTUGAS PEMBEKAL</th>
                            <th>RUJUKAN</th>
                            <th>STATUS PEMBEKAL</th>
                            <th>TINDAKAN</th>
                        </tr>
                    </thead>
                    <tbody>

                        @forelse ($senaraiPembekal as $item)
                        <tr>
                            <td>{{ $item->id }}</td>
                            <td>{{ $item->nama }}</td>
                            <td>{{ $item->kod }}</td>
                            <td>{{ $item->alamat }}</td>
                            <td>{{ $item->telefon }}</td>
                            <td>{{ $item->fax }}</td>
                            <td>{{ $item->tahun }}</td>
                            <td>{{ $item->pegawai_bertugas }}</td>
                            <td>{{ $item->rujukan }}</td>
                            <td>{{ $item->status }}</td>
                            <td align="center">
                                <a href="{{ route('pembekal.edit', $item->id) }}" class="btn btn-info">KEMASKINI</a>

                                <!-- Button trigger modal -->
                                <!--<button type="button" class="btn btn-danger" data-bs-toggle="modal" data-bs-target="#modal-delete-{{ $item->id }}">
                                    HAPUS
                                </button>

                                <!-- Modal -->
                               <!-- <div class="modal fade" id="modal-delete-{{ $item->id }}" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
                                    <form method="POST" action="{{ route('pembekal.destroy', $item->id) }}">
                                        @csrf
                                        @method('DELETE')
                                    <div class="modal-dialog">
                                    <div class="modal-content">
                                        <div class="modal-header">
                                        <h1 class="modal-title fs-5" id="exampleModalLabel">Pengesahan</h1>
                                        <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                                        </div>
                                        <div class="modal-body">
                                            Adakah anda bersetuju untuk menghapuskan data ini?
                                        </div>
                                        <div class="modal-footer">
                                        <button type="button" class="btn btn-secondary" data-bs-dismiss="modal">CLOSE</button>
                                        <button type="button" class="btn btn-danger">DELETE</button>
                                        </div>
                                    </div>
                                    </div>
                                    </form>
                                </div> -->

                            </td>
                        </tr>
                        @empty
                        <tr>
                            <td colspan="9">TIADA REKOD</td>
                        </tr>
                        @endforelse

                    </tbody>

                </table>

            </div>
        </div>

    </div>
</section>

@endsection
