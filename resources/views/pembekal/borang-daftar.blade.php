@extends('layouts.induk')

@section('kod_css')
<link href="https://cdn.jsdelivr.net/npm/select2@4.1.0-rc.0/dist/css/select2.min.css" rel="stylesheet" />
@endsection

@push('script_extra')
<script src="https://cdn.jsdelivr.net/npm/select2@4.1.0-rc.0/dist/js/select2.min.js"></script>

<script>
    $("#select-peranan").select2({
        tags: true
    });
</script>
@endpush

@section('content-utama')

<div class="content-header">
    <div class="container-fluid">
        <div class="row mb-2">
        <div class="col-sm-6">
            <h1 class="m-0">Borang Pendaftaran Pembekal</h1>
        </div><!-- /.col -->
        <div class="col-sm-6">
            <ol class="breadcrumb float-sm-right">
            <li class="breadcrumb-item"><a href="/dashboard">Dashboard</a></li>
            <li class="breadcrumb-item active">Borang Pendaftaran Pembekal</li>
            </ol>
        </div><!-- /.col -->
        </div><!-- /.row -->
    </div><!-- /.container-fluid -->
</div>
<!-- /.content-header -->
<section class="content">

    <div class="container-fluid">

        <form method="POST" action="{{ route('pembekal.store') }}">
            <div class="card">
                <div class="card-body">
                    @csrf

                    <div class="row">

                        <div class="col-md-6">

                            <div class="mb-3">
                                <label class="form-label">KOD PEMBEKAL</label>
                                <input type="text" class="form-control @error('kod') is-invalid @enderror" name="kod" value="{{ old('kod') }}" required>
                                @error('kod')
                                <div class="invalid-feedback">
                                    {{ $message }}.
                                </div>
                                @enderror
                            </div>

                        </div>
                        <div class="col-md-6">

                            <div class="mb-3">
                                <label class="form-label">NAMA PEMBEKAL</label>
                                <input type="text" class="form-control @error('nama') is-invalid @enderror" name="nama" value="{{ old('nama') }}" required>
                                @error('nama')
                                <div class="invalid-feedback">
                                    {{ $message }}.
                                </div>
                                @enderror
                            </div>

                        </div>

                    </div>

                    <div class="row">

                        <div class="col-md-6">

                            <div class="mb-3">
                                <label class="form-label">ALAMAT PEMBEKAL</label>
                                <input type="alamat" class="form-control @error('alamat') is-invalid @enderror" name="alamat" value="{{ old('alamat') }}" required>
                                @error('alamat')
                                <div class="invalid-feedback">
                                    {{ $message }}.
                                </div>
                                @enderror
                            </div>

                        </div>
                        <div class="col-md-6">

                            <div class="mb-3">
                                <label class="form-label">NO. TELEFON</label>
                                <input type="text" class="form-control @error('telefon') is-invalid @enderror" name="telefon" value="{{ old('telefon') }}">
                                @error('telefon')
                                <div class="invalid-feedback">
                                    {{ $message }}.
                                </div>
                                @enderror
                            </div>

                        </div>

                    </div>

                    <div class="row">
                        <div class="col-md-6">

                            <div class="mb-3">
                            <label class="form-label">NO. FAX</label>
                                <input type="text" class="form-control @error('fax') is-invalid @enderror" name="fax" value="{{ old('fax') }}">
                                @error('fax')
                                <div class="invalid-feedback">
                                    {{ $message }}.
                                </div>
                                @enderror
                            </div>

                        </div>
                        <div class="col-md-6">

                            <div class="mb-3">
                            <label class="form-label">TAHUN</label>
                                <input type="text" class="form-control @error('tahun') is-invalid @enderror" name="tahun" value="{{ old('tahun') }}">
                                @error('tahun')
                                <div class="invalid-feedback">
                                    {{ $message }}.
                                </div>
                                @enderror
                            </div>

                        </div>

                    </div>

                    <div class="row">
                        <div class="col-md-6">

                            <div class="mb-3">
                            <label class="form-label">PEGAWAI BERTUGAS (PEMBEKAL)</label>
                                <input type="text" class="form-control @error('pegawai_bertugas') is-invalid @enderror" name="pegawai_bertugas" value="{{ old('pegawai_bertugas') }}">
                                @error('pegawai_bertugas')
                                <div class="invalid-feedback">
                                    {{ $message }}.
                                    </div>
                                @enderror
                            </div>

                        </div>
                        

                    </div>

                    <div class="row">
                        <div class="col-md-6">

                            <div class="mb-3">
                            <div class="mb-3">
                            <label class="form-label">RUJUKAN</label>
                                <input type="text" class="form-control @error('rujukan') is-invalid @enderror" name="rujukan" value="{{ old('rujukan') }}">
                                @error('rujukan')
                                <div class="invalid-feedback">
                                    {{ $message }}.
                                </div>
                                @enderror
                            </div>

                            </div>

                        </div>
                        <div class="col-md-6">

                            <div class="mb-3">
                                <label class="form-label">STATUS</label>

                                <div class="form-check">
                                    <input class="form-check-input" type="radio" name="status" value="AKTIF" {{ old('status') == 1 ? 'checked' : NULL }}>
                                    <label class="form-check-label">
                                        AKTIF
                                    </label>
                                </div>
                                <div class="form-check">
                                    <input class="form-check-input" type="radio" name="status" value="TIDAK AKTIF" {{ old('status') == 0 ? 'checked' : NULL }}>
                                    <label class="form-check-label">
                                        TIDAK AKTIF
                                    </label>
                                </div>

                            </div>

                        </div> 

                    </div>

                </div>
                <div class="card-footer">
                    <button type="submit" class="btn btn-primary">DAFTAR</button>
                </div>
            </div>
        </form>

    </div>

</section>

@endsection
