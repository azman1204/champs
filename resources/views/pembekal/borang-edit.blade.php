@extends('layouts.induk')

@section('content-utama')

<div class="content-header">
    <div class="container-fluid">
        <div class="row mb-2">
        <div class="col-sm-6">
            <h1 class="m-0">Borang Kemaskini Pembekal</h1>
        </div><!-- /.col -->
        <div class="col-sm-6">
            <ol class="breadcrumb float-sm-right">
            <li class="breadcrumb-item"><a href="/dashboard">Dashboard</a></li>
            <li class="breadcrumb-item active">Borang Kemaskini Pembekal</li>
            </ol>
        </div><!-- /.col -->
        </div><!-- /.row -->
    </div><!-- /.container-fluid -->
</div>
<!-- /.content-header -->
<section class="content">

    <div class="container-fluid">

        <form method="POST" action="{{ route('pembekal.update', $pembekal->id) }}">
            @csrf
            @method('GET')
            <div class="card">
                <div class="card-body">

                        <div class="row">

                            <div class="col-md-6">

                                <div class="mb-3">
                                    <label class="form-label">KOD PEMBEKAL</label>
                                    <input type="text" class="form-control @error('kod') is-invalid @enderror" name="kod" value="{{ $pembekal->kod }}" required>
                                    @error('kod')
                                    <div class="invalid-feedback">
                                        {{ $message }}.
                                    </div>
                                    @enderror
                                </div>

                            </div>
                            <div class="col-md-6">

                                <div class="mb-3">
                                    <label class="form-label">NAMA PEMBEKAL</label>
                                    <input type="text" class="form-control @error('nama') is-invalid @enderror" name="nama" value="{{ $pembekal->nama }}" required>
                                    @error('nama')
                                    <div class="invalid-feedback">
                                        {{ $message }}.
                                    </div>
                                    @enderror
                                </div>

                            </div>

                        </div>

                        <div class="row">

                            <div class="col-md-6">

                                <div class="mb-3">
                                    <label class="form-label">ALAMAT PEMBEKAL</label>
                                    <input type="alamat" class="form-control" name="alamat" value="{{ $pembekal->alamat }}" required>
                                </div>

                            </div>
                            <div class="col-md-6">

                                <div class="mb-3">
                                    <label class="form-label">NO. TELEFON</label>
                                    <input type="text" class="form-control" name="telefon" value="{{ $pembekal->telefon }}">
                                </div>

                            </div>

                        </div>

                        <div class="row">
                            <div class="col-md-6">

                                <div class="mb-3">
                                    <label class="form-label">NO. FAX</label>
                                    <input type="text" class="form-control" name="fax" value="{{ $pembekal->fax }}">
                                </div>

                            </div>
                            <div class="col-md-6">

                                <div class="mb-3">
                                    <label class="form-label">TAHUN</label>
                                    <input type="text" class="form-control" name="tahun" value="{{ $pembekal->tahun }}">
                                </div>

                            </div>

                        </div>

                        <div class="row">
                            <div class="col-md-6">

                                <div class="mb-3">
                                    <label class="form-label">KUOTA (RM)</label>
                                    <input type="text" class="form-control" name="kuota" value="{{ $pembekal->kuota }}">

                                </div>

                            </div>
                            <div class="col-md-6">

                                <div class="mb-3">
                                <label class="form-label">PEGAWAI BERTUGAS (PEMBEKAL)</label>
                                    <input type="text" class="form-control @error('pegawai_bertugas') is-invalid @enderror" name="pegawai_bertugas" value="{{ $pembekal->pegawai_bertugas }}" required>
                                    @error('pegawai_bertugas')
                                    <div class="invalid-feedback">
                                        {{ $message }}.
                                    </div>
                                    @enderror

                                </div>

                            </div>

                        </div>

                        <div class="row">
                            <div class="col-md-6">

                                <div class="mb-3">
                                <label class="form-label">RUJUKAN</label>
                                    <input type="text" class="form-control @error('rujukan') is-invalid @enderror" name="rujukan" value="{{ $pembekal->rujukan }}" required>
                                    @error('rujukan')
                                    <div class="invalid-feedback">
                                        {{ $message }}.
                                    </div>
                                    @enderror

                                </div>

                            </div>
                            <div class="col-md-6">

                                <div class="mb-3">
                                    <label class="form-label">STATUS</label>

                                    <div class="form-check">
                                        <input class="form-check-input" type="radio" name="status" value="AKTIF" {{-- $pembekal->status== 1 ? 'checked' : NULL --}}>
                                        <label class="form-check-label">
                                        AKTIF
                                        </label>
                                    </div>
                                    <div class="form-check">
                                        <input class="form-check-input" type="radio" name="status" value="TIDAK AKTIF" {{-- $pembekal->status == 0 ? 'checked' : NULL --}}>
                                        <label class="form-check-label">
                                        TIDAK AKTIF
                                        </label>
                                    </div>

                                </div>

                            </div>

                        </div>




                </div>
                <div class="card-footer">
                    <button type="submit" class="btn btn-primary">SIMPAN</button>
                </div>
            </div>
        </form>

    </div>

</section>

@endsection
