<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\SkimController;
use App\Http\Controllers\LogoutController;
use App\Http\Controllers\PembekalController;
use App\Http\Controllers\PenggunaController;
use App\Http\Controllers\TuntutanController;
use App\Http\Controllers\Auth\LoginController;
use App\Http\Controllers\LaporanController;
use App\Http\Controllers\TuntutanPrintController;

Route::get('/pwd', function() {
    return \Hash::make('1234');
});

Route::redirect('/', '/login');

// Fungsi authentication
Route::get('login', [LoginController::class, 'borangLogin'])->name('login');
Route::post('login', [LoginController::class, 'authenticate'])->name('login.authenticate');

// Ruangan pengguna selepas login
Route::group(['middleware' => 'auth'], function () {

    Route::get('dashboard', fn() => view('dashboard'))->name('dashboard');

    // Module Pengguna
    Route::any('pengguna', [PenggunaController::class, 'paparSenarai'])->name('users.index');
    Route::get('pengguna/{id}/detail', [PenggunaController::class, 'paparDetail'])->name('users.detail');
    Route::get('pengguna/daftar', [PenggunaController::class, 'paparBorangDaftar'])->name('users.create');
    Route::post('pengguna/daftar', [PenggunaController::class, 'terimaDataPendaftaran'])->name('users.store');
    Route::post('pengguna/{id}', [PenggunaController::class, 'paparPengguna'])->name('users.show');
    Route::get('pengguna/{id}/edit', [PenggunaController::class, 'paparBorangEdit'])->name('users.edit');
    Route::get('pengguna/{id}', [PenggunaController::class, 'terimaDataEdit'])->name('users.update');
    Route::delete('pengguna/{id}', [PenggunaController::class, 'hapusData'])->name('users.destroy');

    // Module Pembekal
    Route::get('pembekal', [PembekalController::class, 'paparSenarai'])->name('pembekal.index');
    Route::get('pembekal/daftar', [PembekalController::class, 'paparBorangDaftar'])->name('pembekal.create');
    Route::post('pembekal/daftar', [PembekalController::class, 'terimaDataPendaftaran'])->name('pembekal.store');
    Route::post('pembekal/{id}', [PembekalController::class, 'paparPembekal'])->name('pembekal.show');
    Route::get('pembekal/{id}/edit', [PembekalController::class, 'paparBorangEdit'])->name('pembekal.edit');
    Route::get('pembekal/{id}', [PembekalController::class, 'terimaDataEdit'])->name('pembekal.update');
    Route::delete('pembekal/{id}', [PembekalController::class, 'hapusData'])->name('pembekal.destroy');

    // Module Skim
    Route::get('skim', [SkimController::class, 'paparSenarai'])->name('skim.index');
    Route::get('skim/daftar', [SkimController::class, 'paparBorangDaftar'])->name('skim.create');
    Route::post('skim/daftar', [SkimController::class, 'terimaDataPendaftaran'])->name('skim.store');
    Route::post('skim/{id}', [SkimController::class, 'paparSkim'])->name('skim.show');
    Route::get('skim/{id}/edit', [SkimController::class, 'paparBorangEdit'])->name('skim.edit');
    Route::get('skim/{id}', [SkimController::class, 'terimaDataEdit'])->name('skim.update');
    Route::delete('skim/{id}', [SkimController::class, 'hapusData'])->name('skim.destroy');

    // Module Tuntutan
    Route::post('tuntutan/datatables', [TuntutanController::class, 'datatables'])->name('tuntutan.datatables');
    Route::get('tuntutan/daftar', [TuntutanController::class, 'paparBorangDaftar'])->name('tuntutan.create');
    Route::post('tuntutan/daftar', [TuntutanController::class, 'terimaDataPendaftaran'])->name('tuntutan.store');
    Route::get('tuntutan/{id}/edit', [TuntutanController::class, 'paparBorangEdit'])->name('tuntutan.edit');
   // Route::get('tuntutan/{id}', [TuntutanController::class, 'terimaDataEdit'])->name('tuntutan.update');
    Route::get('tuntutan/{tuntutan}/print', [TuntutanPrintController::class, 'print'])->name('tuntutan.print');

    Route::resource('tuntutan', TuntutanController::class);

    Route::get('logout', [LogoutController::class, 'logout'])->name('logout');

    Route::get('sub-pembekal/{pembekal_id}', [PembekalController::class, 'subPembekal']);

    Route::any('laporan-tuntutan',
    [LaporanController::class, 'tuntutan']);

});
