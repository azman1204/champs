DROP TABLE pembekal_sub;

CREATE TABLE pembekal_sub (
  id INT NOT null PRIMARY KEY AUTO_INCREMENT,
  nama VARCHAR(255),
  kategori CHAR(3), /** PGL, PBK **/
  pembekal_id INT
);

INSERT INTO pembekal_sub(nama, kategori, pembekal_id) VALUES('PPK Kuantan', 'PBK', 1);
INSERT INTO pembekal_sub(nama, kategori, pembekal_id) VALUES('PPK Pekan','PBK', 1);
INSERT INTO pembekal_sub(nama, kategori, pembekal_id) VALUES('PPK Temerloh', 'PBK', 1);
INSERT INTO pembekal_sub(nama, kategori) VALUES('ABC SDN BHD', 'PGL');

SELECT * FROM pembekal_sub;

-- -------------------------------------------------------
DROP TABLE if exists kontrak ;

CREATE TABLE kontrak (
  id INT NOT null PRIMARY KEY AUTO_INCREMENT,
  nama VARCHAR(255),
  jum_peruntukan NUMERIC(13,2)
);

INSERT INTO kontrak(nama, jum_peruntukan) VALUES('Inisiatif tanaman padi 2021-2023', 100000000.00);
INSERT INTO kontrak(nama, jum_peruntukan) VALUES('Inisiatif benih padi sah', 50000000.00);
SELECT * FROM kontrak;

-- -------------------------------------------------------
DROP TABLE if exists penyaluran ;

CREATE TABLE penyaluran (
  id INT NOT NULL PRIMARY KEY AUTO_INCREMENT,
  kontrak_id INT,
  program_id INT,
  skim_id INT,
  jum_kontrak NUMERIC(13,2)
);

INSERT INTO penyaluran(kontrak_id, program_id, skim_id, jum_kontrak) VALUES 
(1,1,1,25000000.00),(1,1,2,35000000.00);
SELECT * FROM penyaluran;

-- -------------------------------------------------------
DROP TABLE if exists penyaluran_pembekal ;

CREATE TABLE penyaluran_pembekal (
  id INT NOT NULL PRIMARY KEY AUTO_INCREMENT,
  penyaluran_id INT NOT null,
  pembekal_id INT NOT null,
  jum_kontrak NUMERIC(13,2),
  kuota NUMERIC(7,2)
);

INSERT INTO penyaluran_pembekal(penyaluran_id, pembekal_id, jum_kontrak) VALUES 
(1,1,5000000.00),(1,2,3000000.00);

SELECT * FROM penyaluran_pembekal;

-- -------------------------------------------------------
DELETE FROM tuntutan_statuses;
DELETE FROM tuntutan_panels;
DELETE FROM tuntutan;
DROP TABLE if exists tuntutan ;

CREATE TABLE tuntutan (
  id INT NOT NULL PRIMARY KEY AUTO_INCREMENT,
  pp_id INT NOT NULL,
  jum_tuntutan NUMERIC(13,2) NOT null,
  tkh_tuntutan DATE NOT null,
  tkh_tuntutan_baru DATE,
  kuantiti NUMERIC(10,2),
  `status` INT NOT NULL, -- 1=baru, 2=disemak, 3=diperku, 4=disahkan, 5=dibayar, 6=disemak semula
  penyemak_id INT,
  peraku_id INT,
  pengesah_id INT,
  tkh_semak DATE,
  tkh_peraku DATE,
  tkh_pengesahan DATE,
  tkh_bayar DATE,
  no_rujukan_fail VARCHAR(255),
  created_at DATETIME,
  updated_at DATETIME
);

DELETE FROM tuntutan;
INSERT INTO tuntutan(pp_id, jum_tuntutan, `status`, tkh_tuntutan, kuantiti) VALUES 
(1,25000.00, 1, '2023-01-01', 10000),(1,35000.00, 1, '2023-02-01', 20000);

SELECT * FROM tuntutan;


-- -------------------------------------------------------
DROP TABLE if exists skims;
DROP TABLE if exists skim;

CREATE TABLE skim (
  id INT NOT NULL PRIMARY KEY AUTO_INCREMENT,
  nama VARCHAR(255) NOT null,
  program_id INT NOT NULL,
  create_at DATETIME,
  updated_at DATETIME
);

INSERT INTO skim(nama, program_id) VALUES 
('Skim Bantuan Padi', 1),('Bantuan Pesawah', 1), ('Tabung Bencana Tanaman Padi', 1),
('Bantuan Pesawah', 1), ('SIPP Kapur', 1), ('SIPP Pembajakan', 1),
('SIPP Baja Tambahan NPK', 1);

SELECT * FROM skim;

-- -------------------------------------------------------
DROP TABLE if exists program;

CREATE TABLE program (
  id INT NOT NULL PRIMARY KEY AUTO_INCREMENT,
  nama VARCHAR(255),
  create_at DATETIME,
  updated_at DATETIME
);

INSERT INTO program(nama) VALUES 
('Skim Bantuan Padi'),('Bantuan Pesawah'), ('Tabung Bencana Tanaman Padi'),
('Bantuan Pesawah'), ('SIPP Kapur'), ('SIPP Pembajakan'),
('SIPP Baja Tambahan NPK');

SELECT * FROM program;

-- -------------------------------------------------------
DROP TABLE if exists kuota;

CREATE TABLE kuota (
  id INT NOT NULL PRIMARY KEY AUTO_INCREMENT,
  pp_id INT NOT NULL,
  tkh_kuota DATE NOT NULL,
  jum_kuota NUMERIC(7,2) NOT NULL,
  create_at DATETIME,
  updated_at DATETIME
);

INSERT INTO kuota(pp_id, tkh_kuota, jum_kuota) VALUES 
(1, '2023-01-01', 7000.00),(1, '2023-03-01', 8000.00);

SELECT * FROM kuota;

-- -------------------------------------------------------
DROP TABLE if exists ref;

CREATE TABLE ref (
  id INT NOT NULL PRIMARY KEY AUTO_INCREMENT,
  cat VARCHAR(50) NOT NULL,
  `code` VARCHAR(25) NOT NULL,
  descr VARCHAR(150) NOT NULL
);

INSERT INTO ref(cat, `code`, descr) VALUES 
('sts-tuntutan', '1', 'Baru'),('sts-tuntutan', '5', 'Bayar'),
('sts-aktif', 'A', 'Aktif'),('sts-aktif', 'T', 'Tidak Aktif');

SELECT * FROM ref;