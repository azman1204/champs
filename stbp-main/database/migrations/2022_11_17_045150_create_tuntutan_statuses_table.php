<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('tuntutan_statuses', function (Blueprint $table) {
            $table->id();
            $table->unsignedBigInteger('tuntutan_id');
            $table->string('status');
            $table->timestamps();

            $table->foreign('tuntutan_id')->references('id')->on('tuntutan');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('tuntutan_statuses');
    }
};
