<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('users', function (Blueprint $table) {
            $table->id();
            $table->string('no_kp', 12)->unique();
            $table->string('password');
            $table->string('nama');
            $table->bigInteger('jawatan_id')->unsigned();
            $table->unsignedBigInteger('bahagian_id');
            $table->string('email')->unique();
            $table->timestamp('email_verified_at')->nullable();
            $table->boolean('status')->default(0);
            $table->rememberToken();
            $table->timestamps();

            // Relation kepada table berkaitan
            $table->foreign('jawatan_id')->references('id')->on('jawatan');
            $table->foreign('bahagian_id')->references('id')->on('bahagian');
            
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('users');
    }
};
