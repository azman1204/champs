<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Spatie\Permission\Models\Permission;
use Illuminate\Database\Console\Seeds\WithoutModelEvents;

class PermissionSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        // Query Builder
        // DB::table('permissions')->insert();
        // Model
        // Super Admin permission
        Permission::create([
            'name' => 'superadmin.all.areas'
        ]);

        // Halaman Tuntutan
        Permission::create([
            'name' => 'tuntutan.view'
        ]);

        Permission::create([
            'name' => 'tuntutan.create'
        ]);

        Permission::create([
            'name' => 'tuntutan.edit'
        ]);

        Permission::create([
            'name' => 'tuntutan.delete'
        ]);

        // Halaman Pengguna
        Permission::create([
            'name' => 'users.view'
        ]);

        Permission::create([
            'name' => 'users.create'
        ]);

        Permission::create([
            'name' => 'users.edit'
        ]);

        Permission::create([
            'name' => 'users.delete'
        ]);

        // Halaman Pembekal
        Permission::create([
            'name' => 'pembekal.view'
        ]);

        Permission::create([
            'name' => 'pembekal.create'
        ]);

        Permission::create([
            'name' => 'pembekal.edit'
        ]);

        Permission::create([
            'name' => 'pembekal.delete'
        ]);

        // Halaman Skim
        Permission::create([
            'name' => 'skim.view'
        ]);

        Permission::create([
            'name' => 'skim.create'
        ]);

        Permission::create([
            'name' => 'skim.edit'
        ]);

        Permission::create([
            'name' => 'skim.delete'
        ]);

        // Halaman Jawatan
        Permission::create([
            'name' => 'jawatan.view'
        ]);

        Permission::create([
            'name' => 'jawatan.create'
        ]);

        Permission::create([
            'name' => 'jawatan.edit'
        ]);

        Permission::create([
            'name' => 'jawatan.delete'
        ]);

        // Halaman Bahagian
        Permission::create([
            'name' => 'bahagian.view'
        ]);

        Permission::create([
            'name' => 'bahagian.create'
        ]);

        Permission::create([
            'name' => 'bahagian.edit'
        ]);

        Permission::create([
            'name' => 'bahagian.delete'
        ]);

        // Halaman Bayaran
        Permission::create([
            'name' => 'bayaran.view'
        ]);

        Permission::create([
            'name' => 'bayaran.create'
        ]);

        Permission::create([
            'name' => 'bayaran.edit'
        ]);

        Permission::create([
            'name' => 'bayaran.delete'
        ]);

        // Halaman Peranan/Role
        Permission::create([
            'name' => 'peranan.view'
        ]);

        Permission::create([
            'name' => 'peranan.create'
        ]);

        Permission::create([
            'name' => 'peranan.edit'
        ]);

        Permission::create([
            'name' => 'peranan.delete'
        ]);
    }
}
