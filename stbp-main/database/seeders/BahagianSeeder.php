<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
use Illuminate\Database\Console\Seeds\WithoutModelEvents;

class BahagianSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {

        DB::table('bahagian')->insert([
            'nama' => 'Padi & Beras',
        ]);

        DB::table('bahagian')->insert([
            'nama' => 'Urusetia Ketua Pengarah',
        ]);
    }
}
