<?php

namespace App\Http\Controllers;

use App\Models\Skim;
use App\Models\User;
use App\Models\Pembekal;
use App\Models\Tuntutan;
use Illuminate\Http\Request;
use App\Models\TuntutanPanel;
use Yajra\DataTables\DataTables;
use Illuminate\Support\Facades\DB;

class TuntutanController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */

    public function __construct()
    {
        $this->middleware('permission:superadmin.all.areas|tuntutan.view', ['only' => ['paparSenarai']]);
        $this->middleware('permission:superadmin.all.areas|tuntutan.create', ['only' => ['paparBorangDaftar', 'terimaDataPendaftaran']]);
        $this->middleware('permission:superadmin.all.areas|tuntutan.edit', ['only' => ['paparBorangEdit', 'terimaDataEdit']]);
        $this->middleware('permission:superadmin.all.areas|tuntutan.delete', ['only' => ['hapusData']]);
    }


    public function index()
    {
        // $senaraiTuntutan = Tuntutan::with('pembekal', 'skim')
        // //->orderBy('id', 'desc')
        // ->latest()
        // ->paginate(5);

        //return view('tuntutan.index', compact('senaraiTuntutan'));
        return view('tuntutan.index');
    }

    public function datatables()
    {
        $query = Tuntutan::query()->with('pembekal', 'skim');

        return DataTables::of($query)
        ->addColumn('tindakan', function ($tuntutan) {
            return view('tuntutan.datatables-tindakan', compact('tuntutan'));
        })
        ->make(true);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $senaraiPembekal = Pembekal::select('id', 'nama')->get();
        $senaraiSkim = Skim::select('id', 'nama')->get();
        $senaraiPanel = User::select('id', 'nama')->get();

        return view('tuntutan.create', compact('senaraiPembekal', 'senaraiSkim', 'senaraiPanel'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $data = $request->all();

         DB::transaction(function () use ($data) {
             $tuntutan = Tuntutan::create($data);

            //  Cara 1 Simpan data panel penyemak
            //  TuntutanPanel::create([
            //      'tuntutan_id' => $tuntutan->id,
            //      'user_id' > $data['user_id']
            //  ]);

             // Cara 2 Simpan data panel penyemak
         //     $panel = new TuntutanPanel;
          //    $panel->tuntutan_id = $tuntutan->id,
          //    $panel->user_id = $data['user_id'];l
           //   $panel->save();

             // Cara 3 Simpan data panel penyemak menerusi relation di Tuntutan
           //$tuntutan->panel()->create($data);
           //$tuntutan->status()->create([
           //      'status' => Tuntutan::STATUS_TUNTUTAN_BARU
           //  ]);
          });

//Simpan daftar tuntutan
function terimaDataPendaftaran(Request $request)
{
    // Proses validasi dan dapatkan semua data yang di-validate
    $data = $request->validate([
        'no_fail' => ['min:3'],
        'skim_id' => ['min:3'],
        'pembekal_id' => ['min:3'],
        'tahun' => ['integer'],
        'tarikh_tuntutan' => ['min:3'],
        'tarikh_tuntutan_baru' => ['min:3'],
        'jumlah_peruntukan' => ['min:3'],
        'baki_peruntukan' => ['min:3'],
        'kuota' => ['min:3'],
        
    ]);

    // Beri response redirect ke senarai skim
    return redirect()->route('tuntutan.index')
    ->with('alert-success', 'Rekod berjaya ditambah');
}

        //try {
         //   DB::beginTransaction();

          //  $tuntutan = Tuntutan::create($data);

          //  $tuntutan->panel()->create($data);

          //  $tuntutan->status()->create([
          //      'status' => Tuntutan::STATUS_TUNTUTAN_BARU
          //  ]);

          //  DB::commit();

       // } catch (\Throwable $e) {


          //  DB::rollBack();

          //  return redirect()->route('tuntutan.index')
            // ->with('alert-error', 'Rekod tidak berjaya ditambah.' . $e->getMessage());
         //   ->with('alert-error', 'Rekod tidak berjaya ditambah. Ada masalah dengan kemasukan data. Sila hubungi admin.');

        //}

        return redirect()->route('tuntutan.index')
        ->with('alert-success', 'Rekod Tuntutan berjaya didaftarkan.');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Tuntutan  $tuntutan
     * @return \Illuminate\Http\Response
     */
    public function show(Tuntutan $tuntutan)
    {
        if (auth()->user()->hasAnyRole('Ketua Panel BPB'))
        {
            return view('tuntutan.detail-panel', compact('tuntutan'));
        }

        return view('tuntutan.detail', compact('tuntutan'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Tuntutan  $tuntutan
     * @return \Illuminate\Http\Response
     */
    function paparBorangEdit($id)
    {
        $tuntutan = Tuntutan::findOrFail($id);

        $senaraiTuntutan = Tuntutan::select('id')->get();

        return view('tuntutan.borang-edit', compact('tuntutan'));

    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Tuntutan  $tuntutan
     * @return \Illuminate\Http\Response
     */
   // function terimaDataEdit(Request $request, $id)
   // {
        // Proses validasi dan dapatkan semua data yang di-validate
      //  $data = $request->validate([
      //      'no_fail' => ['min:3'],
      //      'skim_id' => ['min:3'],
      //      'pembekal_id' => ['min:3'],
      //      'tahun' => ['integer'],
      //      'tarikh_tuntutan' => ['min:3'],
     //       'tarikh_tuntutan_baru' => ['min:3'],
      //      'jumlah_peruntukan' => ['min:3'],
      //      'baki_peruntukan' => ['min:3'],
      //      'kuota' => ['min:3'],
       //     'dokumen' => ['min:3'],
       //     'catatan' => ['min:3'],
       //     'semakan_panel' => ['min:3'],

      //  ]);

         // Kemaskini data ke dalam table skim
       //  $tuntutan = tuntutan::findOrFail($id);
       //  $tuntutan->update($id);
 
         // Beri response redirect ke senarai skim
      //   return redirect()->route('tuntutan.index')
      //   ->with('alert-success', 'Rekod berjaya dikemaskini');
   // }


    public function update(Request $request, Tuntutan $tuntutan)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Tuntutan  $tuntutan
     * @return \Illuminate\Http\Response
     */
    public function destroy(Tuntutan $tuntutan)
    {
        //
    }
}
