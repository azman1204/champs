<?php

namespace App\Http\Controllers;

use App\Models\Pembekal;
use Illuminate\Http\Request;

class PembekalController extends Controller
{
    public function __construct()
    {
        $this->middleware('permission:superadmin.all.areas|pembekal.view', ['only' => ['paparSenarai']]);
        $this->middleware('permission:superadmin.all.areas|pembekal.create', ['only' => ['paparBorangDaftar', 'terimaDataPendaftaran']]);
        $this->middleware('permission:superadmin.all.areas|pembekal.edit', ['only' => ['paparBorangEdit', 'terimaDataEdit']]);
        $this->middleware('permission:superadmin.all.areas|pembekal.delete', ['only' => ['hapusData']]);
    }

    //papar senarai
    function paparSenarai()
    {
        $senaraiPembekal = Pembekal::paginate(10);

        return view('pembekal.index', compact('senaraiPembekal'));
    }

    // Function untuk memaparkan borang pendaftaran pengguna
    function paparBorangDaftar()
    {
        $senaraiPembekal = Pembekal::select('id', 'nama')->get();

        return view('pembekal.borang-daftar', compact('senaraiPembekal'));
    }

    //Simpan daftar pembekal
    function terimaDataPendaftaran(Request $request)
    {
        // Proses validasi dan dapatkan semua data yang di-validate
        $data = $request->validate([
            'kod' => ['required', 'min:3'],
            'nama' => ['required', 'min:3'],
            'alamat' => ['required', 'min:3'],
            'telefon' => ['nullable', 'sometimes'],
            'fax' => ['nullable', 'sometimes'],
            'tahun' => ['required', 'integer'],
            'pegawai_bertugas' => ['required'],
            'status' => ['required'],
            'rujukan' => ['required', 'min:3'],
            //'peranan_id' => ['required', 'integer'],
        ]);

        // Simpan data ke dalam table pembekal
        $pembekal = Pembekal::create($data);

        // Attach/assign peranan/role kepada user
        //foreach ($request->peranan as $perananId)
        //{
       //     $role = Role::findById($perananId);

       //     $user->assignRole($role);
      //  }


        // Beri response redirect ke senarai pembekal
        return redirect()->route('pembekal.index')
        ->with('alert-success', 'Rekod berjaya ditambah');
    }

    //papar senarai pembekal

    function paparPembekal($id)
    {
        $pembekal = Pembekal::findOrFail($id);

        return view('pembekal.detail-pembekal', compact('pembekal'));

    }

    //papar borang edit

    function paparBorangEdit($id)
    {
        $pembekal = Pembekal::findOrFail($id);

        $senaraiPembekal = Pembekal::select('id', 'nama')->get();

        return view('pembekal.borang-edit', compact('pembekal'));

    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Pembekal  $pembekal
     * @return \Illuminate\Http\Response
     */
    function terimaDataEdit(Request $request, $id)
    {
        // Proses validasi dan dapatkan semua data yang di-validate
        $data = $request->validate([
            'kod' => ['required', 'min:3'],
            'nama' => ['required', 'min:3'],
            'alamat' => ['required', 'min:3'],
            'telefon' => ['nullable', 'sometimes'],
            'fax' => ['nullable'],
            'tahun' => ['required', 'integer'],
            'pegawai_bertugas' => ['required'],
            'status' => ['required'],
            'rujukan' => ['required', 'min:3'],

        ]);

         // Kemaskini data ke dalam table pembekal
         $pembekal = Pembekal::findOrFail($id);
         $pembekal->update($data);
 
         // Beri response redirect ke senarai pembekal/pengguna
         return redirect()->route('pembekal.index')
         ->with('alert-success', 'Rekod berjaya dikemaskini');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Pembekal  $pembekal
     * @return \Illuminate\Http\Response
     */
    public function destroy(Pembekal $pembekal)
    {
        //
    }

    function hapusData($pembekal)
    {
        $pembekal = Pembekal::findOrFail($pembekal);
        $pembekal->destroy($pembekal);

        // Beri response redirect ke senarai user/pengguna
        return redirect()->route('users.index')
        ->with('alert-success', 'Rekod berjaya dihapuskan');
    }
}
