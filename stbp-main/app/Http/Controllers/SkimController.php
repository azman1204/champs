<?php

namespace App\Http\Controllers;

use App\Models\Skim;
use Illuminate\Http\Request;

class SkimController extends Controller
{
    public function __construct()
    {
        $this->middleware('permission:superadmin.all.areas|skim.view', ['only' => ['paparSenarai']]);
        $this->middleware('permission:superadmin.all.areas|skim.create', ['only' => ['paparBorangDaftar', 'terimaDataPendaftaran']]);
        $this->middleware('permission:superadmin.all.areas|skim.edit', ['only' => ['paparBorangEdit', 'terimaDataEdit']]);
        $this->middleware('permission:superadmin.all.areas|skim.delete', ['only' => ['hapusData']]);
    }

    //papar senarai
    function paparSenarai()
    {
        $senaraiSkim = Skim::paginate(10);

        return view('skim.index', compact('senaraiSkim'));
    }
    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    // Function untuk memaparkan borang pendaftaran pengguna
    function paparBorangDaftar()
    {
        $senaraiSkim = Skim::select('id', 'nama')->get();

        return view('skim.borang-daftar', compact('senaraiSkim'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    //Simpan daftar skim
    function terimaDataPendaftaran(Request $request)
    {
        // Proses validasi dan dapatkan semua data yang di-validate
        $data = $request->validate([
            'vot' => ['required', 'min:3'],
            'nama' => ['required', 'min:3'],
            'tahun' => ['required', 'integer'],
            'jumlah_peruntukan' => ['required'],
            'baki_peruntukan' => ['nullable', 'sometimes'],
            'tarikh_kelulusan' => ['required'],
            'rujukan_kelulusan' => ['required'],
            'catatan' => ['required'],
            
            //'peranan_id' => ['required', 'integer'],
        ]);

        // Simpan data ke dalam table skim
        $skim = skim::create($data);

        // Attach/assign peranan/role kepada user
        //foreach ($request->peranan as $perananId)
        //{
       //     $role = Role::findById($perananId);

       //     $user->assignRole($role);
      //  }


        // Beri response redirect ke senarai skim
        return redirect()->route('skim.index')
        ->with('alert-success', 'Rekod berjaya ditambah');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    //papar senarai skim

    function paparSkim($id)
    {
        $skim = Skim::findOrFail($id);

        return view('skim.detail-skim', compact('skim'));

    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    //papar borang edit

    function paparBorangEdit($id)
    {
        $skim = Skim::findOrFail($id);

        $senaraiSkim = Skim::select('id', 'nama')->get();

        return view('skim.borang-edit', compact('skim'));

    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    function terimaDataEdit(Request $request, $id)
    {
        // Proses validasi dan dapatkan semua data yang di-validate
        $data = $request->validate([
            'vot' => ['required', 'min:3'],
            'nama' => ['required', 'min:3'],
            'tahun' => ['required', 'integer'],
            'jumlah_peruntukan' => ['required'],
            'baki_peruntukan' => ['nullable', 'sometimes'],
            'tarikh_kelulusan' => ['required'],
            'rujukan_kelulusan' => ['required'],
            'catatan' => ['required'],

        ]);

         // Kemaskini data ke dalam table skim
         $skim = Skim::findOrFail($id);
         $skim->update($data);
 
         // Beri response redirect ke senarai skim
         return redirect()->route('skim.index')
         ->with('alert-success', 'Rekod berjaya dikemaskini');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
