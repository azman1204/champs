<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

use Kalnoy\Nestedset\NodeTrait;

class Skim extends Model
{
    use HasFactory;
    use NodeTrait;

    protected $fillable = [
        'nama',
        'vot',
        'tahun',
        'jumlah_peruntukan',
        'baki_peruntukan',
        'tarikh_kelulusan',
        'rujukan_kelulusan',
        'catatan'
    ];
}
