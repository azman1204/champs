<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class TuntutanStatus extends Model
{
    use HasFactory;

    protected $fillable = [
        'tuntutan_id',
        'status'
    ];
}
