<?php

namespace Database\Seeders;

use App\Models\Skim;
use Illuminate\Database\Seeder;
use Illuminate\Database\Console\Seeds\WithoutModelEvents;

class SkimSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Skim::create([
            'nama' => 'Skim Bantuan Padi',
            'vot' => '1-000-500-96001',
            'tahun' => '2022',
            'jumlah_peruntukan' => '10000000',
            'baki_peruntukan' => '10000000',
            'tarikh_kelulusan' => '2022-10-01',
            'rujukan_kelulusan' => NULL,
            'catatan' => NULL
        ]);

        Skim::create([
            'nama' => 'Bantuan Pesawah',
            'vot' => '1-000-500-96002',
            'tahun' => '2022',
            'jumlah_peruntukan' => '50000000',
            'baki_peruntukan' => '50000000',
            'tarikh_kelulusan' => '2022-10-01',
            'rujukan_kelulusan' => NULL,
            'catatan' => NULL
        ]);

        Skim::create([
            'nama' => 'Tabung Bencana Tanaman Padi',
            'vot' => '1-000-500-96003',
            'tahun' => '2022',
            'jumlah_peruntukan' => '80000000',
            'baki_peruntukan' => '80000000',
            'tarikh_kelulusan' => '2022-10-01',
            'rujukan_kelulusan' => NULL,
            'catatan' => NULL
        ]);
    }
}
