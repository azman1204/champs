<?php

namespace Database\Seeders;

use App\Models\Pembekal;
use Illuminate\Database\Seeder;
use Illuminate\Database\Console\Seeds\WithoutModelEvents;


class User extends Authenticatable
{
    use HasApiTokens, HasFactory, Notifiable, HasRoles;

    /**
     * The attributes that are mass assignable.
     *
     * @var array<int, string>
     */
    protected $fillable = [
        'kod',
        'nama',
        'alamat',
        'telefon',
        'fax',
        'kuota',
        'tahun',
        'pegawai_bertugas',
        'rujukan',
        'status'
    ];

        // Bagi role kepada user
       // $user1->assignRole('Pendaftar Pembekal');
       // $user1->assignRole('Pendaftar Pembekal');
    }

