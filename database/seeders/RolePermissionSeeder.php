<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Spatie\Permission\Models\Role;
use Spatie\Permission\Models\Permission;
use Illuminate\Database\Console\Seeds\WithoutModelEvents;

class RolePermissionSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        // Dapatkan role Super Admin
        $superadmin = Role::findByName('Super Admin');

        // Cari permissions untuk di attach kepada role
        $superadminPermissions = Permission::whereIn('name', [
            'superadmin.all.areas'
        ])->get();

        // Attach permissions kepada role superadmin
        $superadmin->syncPermissions($superadminPermissions);

        /////////////////////////////////////////////////////////////////

        // Dapatkan role Ketua panel BPB
        $ketuaPanel = Role::findByName('Ketua Panel BPB');

        // Cari permissions untuk di attach kepada role
        $ketuaPanelPermissions = Permission::whereNotIn('name', [
            'superadmin.all.areas',
            'peranan.view',
            'peranan.create',
            'peranan.edit',
            'peranan.delete'
        ])->get();

        // Attach permissions kepada role Ketua panel BPB
        $ketuaPanel->syncPermissions($ketuaPanelPermissions);

        /////////////////////////////////////////////////////////////////

        // Dapatkan role Panel BPB
        $panelBPB = Role::findByName('Panel BPB');

        // Cari permissions untuk di attach kepada role
        $panelBPBPermissions = Permission::whereNotIn('name', [
            'superadmin.all.areas',
            'peranan.view',
            'peranan.create',
            'peranan.edit',
            'peranan.delete',
            'tuntutan.view',
            'tuntutan.edit',
            'tuntutan.delete'
        ])->get();

        // Attach permissions kepada role Ketua panel BPB
        $ketuaPanel->syncPermissions($ketuaPanelPermissions);

        /////////////////////////////////////////////////////////////////

    }
}
