<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
use Illuminate\Database\Console\Seeds\WithoutModelEvents;

class JawatanSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('jawatan')->insert([
            'nama' => 'Eksekutif Cawangan Pengurusan Dasar ',
        ]);

        DB::table('jawatan')->insert([
            'nama' => 'Penolong Akaun Cawangan Pengurusan Dasar ',
        ]);

        DB::table('jawatan')->insert([
            'nama' => 'Pembantu Ehwal Ekonomi Cawangan Pengurusan Dasar ',
        ]);

        DB::table('jawatan')->insert([
            'nama' => 'Penolong Pengarah Cawangan Pengurusan Dasar ',
        ]);

        DB::table('jawatan')->insert([
            'nama' => 'Ketua Cawangan Pengurusan Dasar ',
        ]);

        DB::table('jawatan')->insert([
            'nama' => 'Eksekutif Cawangan Input Pertanian & Insentif ',
        ]);

        DB::table('jawatan')->insert([
            'nama' => 'Eksekutif Kanan Cawangan Input Pertanian & Insentif ',
        ]);

        DB::table('jawatan')->insert([
            'nama' => 'Penolong Akaun Cawangan Input Pertanian & Insentif ',
        ]);

        DB::table('jawatan')->insert([
            'nama' => 'Pembantu Ehwal Ekonomi  Cawangan Input Pertanian & Insentif ',
        ]);

        DB::table('jawatan')->insert([
            'nama' => 'Penolong Pengarah Cawangan Input Pertanian & Insentif ',
        ]);

        DB::table('jawatan')->insert([
            'nama' => 'Ketua Cawangan Input Pertanian & Insentif ',
        ]);

        DB::table('jawatan')->insert([
            'nama' => 'Pengarah Bahagian Padi & Beras ',
        ]);

        DB::table('jawatan')->insert([
            'nama' => 'Urusetia Ketua Pengarah ',
        ]);

    }
}
