<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Spatie\Permission\Models\Role;
use Illuminate\Database\Console\Seeds\WithoutModelEvents;

class RoleSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Role::create([
            'name' => 'Super Admin'
        ]);

        Role::create([
            'name' => 'Admin BPB'
        ]);

        Role::create([
            'name' => 'Ketua Panel BPB'
        ]);

        Role::create([
            'name' => 'Panel BPB'
        ]);

        Role::create([
            'name' => 'Pendaftar Progam/Skim'
        ]);

        Role::create([
            'name' => 'Pengesahan Daftar Progam/Skim'
        ]);

        Role::create([
            'name' => 'Pendaftar Pembekal'
        ]);

        Role::create([
            'name' => 'Pengesahan Daftar Pembekal'
        ]);

        Role::create([
            'name' => 'Peraku Bayaran'
        ]);

        Role::create([
            'name' => 'Pengesahan Peraku Bayaran'
        ]);

        Role::create([
            'name' => 'Urusetia Ketua Pengarah'
        ]);
    }
}
