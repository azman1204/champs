<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('skims', function (Blueprint $table) {
            $table->id();
            $table->nestedSet();
            $table->string('nama');
            $table->string('vot', 50);
            $table->integer('tahun')->unsigned();
            $table->decimal('jumlah_peruntukan', 16, 2)->default(0);
            $table->decimal('baki_peruntukan', 16, 2)->default(0);
            $table->date('tarikh_kelulusan')->nullable();
            $table->string('rujukan_kelulusan')->nullable();
            $table->text('catatan')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('skims');
    }
};
