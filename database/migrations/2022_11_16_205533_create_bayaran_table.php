<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('bayaran', function (Blueprint $table) {
            $table->id();
            $table->unsignedBigInteger('tuntutan_id');
            $table->unsignedBigInteger('skim_id');
            $table->unsignedBigInteger('pembekal_id');
            $table->text('catatan')->nullable();
            $table->string('status')->nullable();
            $table->timestamps();

            $table->foreign('skim_id')->references('id')->on('skims');
            $table->foreign('pembekal_id')->references('id')->on('pembekal');
            $table->foreign('tuntutan_id')->references('id')->on('tuntutan');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('bayaran');
    }
};
