<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('pembekal', function (Blueprint $table) {
            $table->id();
            $table->string('kod');
            $table->string('nama');
            $table->text('alamat')->nullable();
            $table->string('telefon')->nullable();
            $table->string('fax')->nullable();
            $table->decimal('kuota', 16, 2)->default(0);
            $table->integer('tahun');
            $table->string('pegawai_bertugas')->nullable();
            $table->string('status')->nullable();
            $table->string('rujukan')->nullable();
            $table->timestamps();

            // $table->foreign('pegawai_id')->references('id')->on('users');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('pembekal');
    }
};
