<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('tuntutan', function (Blueprint $table) {
            $table->id();
            $table->string('no_fail');
            $table->unsignedBigInteger('skim_id');
            $table->unsignedBigInteger('pembekal_id');
            $table->integer('tahun');
            $table->date('tarikh_tuntutan')->nullable();
            $table->decimal('jumlah_tuntutan', 16, 2)->default(0);
            $table->decimal('jumlah_peruntukan', 16, 2)->default(0);
            $table->decimal('baki_peruntukan', 16, 2)->default(0);
            $table->decimal('kuota', 16, 2)->default(0);
            $table->timestamps();

            $table->foreign('skim_id')->references('id')->on('skims');
            $table->foreign('pembekal_id')->references('id')->on('pembekal');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('tuntutan');
    }
};
