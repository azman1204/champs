<?php

namespace App\Http\Controllers;

use App\Models\Bahagian;
use App\Models\Cawangan;
use App\Models\Jawatan;
use App\Models\User;
use Illuminate\Http\Request;
use Spatie\Permission\Models\Role;

class PenggunaController extends Controller
{
    public function __construct()
    {
        $this->middleware('permission:superadmin.all.areas|users.view', ['only' => ['paparSenarai', 'paparDetail']]);
        $this->middleware('permission:superadmin.all.areas|users.create', ['only' => ['paparBorangDaftar', 'terimaDataPendaftaran']]);
        $this->middleware('permission:superadmin.all.areas|users.edit', ['only' => ['paparBorangEdit', 'terimaDataEdit']]);
        $this->middleware('permission:superadmin.all.areas|users.delete', ['only' => ['hapusData']]);

    }

    // Function untuk memaparkan senarai pengguna
    function paparSenarai(Request $req) {
        if ($req->has('nama')) {
            // user buat search
            $query = User::whereNotNull('id');
            if (! empty($req->nama)) {
                $nama = $req->nama;
                $query = $query->where('nama', 'like', "%$nama%");
            }

            if (! empty($req->nokp)) {
                $nokp = $req->nokp;
                $query = $query->where('no_kp', "$nokp");
            }

            $senaraiPengguna = $query->paginate(5);
        } else {
            $senaraiPengguna = User::paginate(5);
        }

        //return view('pengguna.index')->with('senaraiPengguna', $senaraiPengguna);
        //return view('pengguna.index', array('senaraiPengguna' => $senaraiPengguna));
        return view('pengguna.index', compact('senaraiPengguna'));
       // return view('pengguna.detail-panel', compact('senaraiBahagian','senaraiJawatan','senaraiPeranan'));
    }

    //Function untuk memaparkan detail pengguna
   function paparDetail()
   {
        $detailPengguna = User::paginate(10);
       // $senaraiBahagian = Bahagian::select('id', 'nama')->get();
        //$senaraiJawatan = Jawatan::select('id', 'nama')->get();
        //$senaraiPeranan = Role::select('id', 'name')->get();
       return view('pengguna.detail', compact('detailPengguna'));
   }

    // Function untuk memaparkan borang pendaftaran pengguna
    function paparBorangDaftar()
    {
        $senaraiBahagian = Bahagian::select('id', 'nama')->get();
        $senaraiJawatan = Jawatan::select('id', 'nama')->get();
        $senaraiPeranan = Role::select('id', 'name')->get();

        return view('pengguna.borang-daftar', compact('senaraiBahagian','senaraiJawatan','senaraiPeranan'));
    }


    function terimaDataPendaftaran(Request $request)
    {
        // Proses validasi dan dapatkan semua data yang di-validate
        $data = $request->validate([
            'no_kp' => ['required', 'min:12', 'unique:users,no_kp'],
            'nama' => ['required', 'min:3'],
            'email' => ['required', 'email:filter', 'unique:users,email'],
            'telefon' => ['nullable', 'sometimes'],
            'password' => ['required', 'min:5', 'confirmed'],
            'jawatan_id' => ['required', 'integer'],
            'bahagian_id' => ['required', 'integer'],
            'status' => ['required'],
            //'peranan_id' => ['required'],
        ]);

        // Simpan data ke dalam table users
        $user = User::create($data);

        // Attach/assign peranan/role kepada user
        foreach ($request->peranan as $perananId)
        {
            $role = Role::findById($perananId);

            $user->assignRole($role);
        }


        // Beri response redirect ke senarai user/pengguna
        return redirect()->route('users.index')
        ->with('alert-success', 'Rekod berjaya ditambah');
    }

    function paparPengguna($id)
    {
        $user = User::findOrFail($id);

        return view('pengguna.detail-pengguna', compact('user', 'senaraiBahagian','senaraiJawatan','senaraiPeranan'));

    }

    function paparBorangEdit($id) {
        $user = User::findOrFail($id);
        $roles = $user->roles->pluck('name', 'id');
        $senaraiBahagian = Bahagian::select('id', 'nama')->get();
        $senaraiJawatan = Jawatan::select('id', 'nama')->get();
        $senaraiPeranan = Role::select('id', 'name')->get();
        $cawangan = Cawangan::pluck('nama', 'id_cawangan');
        $cawangan->prepend('-- Sila Pilih--', '0');
        return view('pengguna.borang-edit',
        compact('user','senaraiBahagian','senaraiJawatan','senaraiPeranan', 'cawangan', 'roles'));
    }

    function terimaDataEdit(Request $request, $id) {
        // die('hello: '.$request->id);
        // Proses validasi dan dapatkan semua data yang di-validate
        $data = $request->validate([
            'no_kp' => ['required', 'min:12', 'unique:users,no_kp,' . $id],
            'nama' => ['required', 'min:3'],
            'email' => ['required', 'email:filter', 'unique:users,email,' . $id],
            'telefon' => ['nullable', 'sometimes'],
            'jawatan_id' => ['required', 'integer'],
            'bahagian_id' => ['required', 'integer'],
            'status' => ['required'],
            //'peranan_id' => ['required'],
            'cawangan_id' => ['required'],
        ]);

        // Semak sekiranya password diisi pada borang kemaskini dan password tidak kosong, maka lakukan validasi
        if (!empty($request->password))
        {
            $request->validate([
                'password' => ['required', 'min:5', 'confirmed']
            ]);

            // Attachkan password kepada $data untuk dikemaskini ke dalam table user
            $data['password'] = $request->input('password');
        }

        // Kemaskini data ke dalam table users
        $user = User::findOrFail($id);
        $user->update($data);

        $user->syncRoles([]); // delete semua role utk user ini
        $roles = $request->input('role_id');
        foreach ($roles as $role) {
            $user->assignRole($role);
        }

        // Beri response redirect ke senarai user/pengguna
        return redirect()->route('users.index')
        ->with('alert-success', 'Rekod berjaya dikemaskini');
    }

    function hapusData($id)
    {
        $user = User::findOrFail($id);
        $user->destroy($user);

        // Beri response redirect ke senarai user/pengguna
        return redirect()->route('users.index')
        ->with('alert-success', 'Rekod berjaya dihapuskan');
    }
}
