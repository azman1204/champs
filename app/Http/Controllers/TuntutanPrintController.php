<?php

namespace App\Http\Controllers;

use App\Models\Tuntutan;
use Illuminate\Http\Request;
use Barryvdh\DomPDF\Facade\Pdf;

class TuntutanPrintController extends Controller
{
    public function print(Request $request, Tuntutan $tuntutan)
    {
        $pdf = Pdf::loadView('tuntutan.cetakan.borang-1', compact('tuntutan'));
        return $pdf->download($tuntutan->no_fail . ' -borang-1.pdf');
    }
}
