<?php

namespace App\Http\Controllers;

use App\Models\Skim;
use App\Models\Tuntutan;
use App\Models\User;
use App\Models\Ref;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Yajra\DataTables\DataTables;

class TuntutanController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */

    public function __construct()
    {
        $this->middleware('permission:superadmin.all.areas|tuntutan.view', ['only' => ['paparSenarai']]);
        $this->middleware('permission:superadmin.all.areas|tuntutan.create', ['only' => ['paparBorangDaftar', 'terimaDataPendaftaran']]);
        $this->middleware('permission:superadmin.all.areas|tuntutan.edit', ['only' => ['paparBorangEdit', 'terimaDataEdit']]);
        $this->middleware('permission:superadmin.all.areas|tuntutan.delete', ['only' => ['hapusData']]);
    }

    // champs.test/tuntutan
    public function index() {
        $status = Ref::where('cat', 'sts-tuntutan')->pluck('descr', 'code');
        $sql = "SELECT t.no_rujukan_fail, t.jum_tuntutan, t.tkh_tuntutan, pp.jum_kontrak, s.nama, t.status
                FROM tuntutan t, penyaluran_pembekal pp, penyaluran p, skim s
                WHERE t.pp_id = pp.id and pp.penyaluran_id = p.id AND p.skim_id = s.id";
        $rows = \DB::select($sql);
        //dd($rows);
        return view('tuntutan.listing', compact('rows', 'status'));
    }

    public function datatables()
    {
        $query = Tuntutan::query()->with('pembekal', 'skim');
        return DataTables::of($query)
            ->addColumn('tindakan', function ($tuntutan) {
                return view('tuntutan.datatables-tindakan', compact('tuntutan'));
            })
            ->make(true);
    }

    public function create()
    {
        //$senaraiPembekal = Pembekal::select('id', 'nama')->get();
        $rows = \DB::select("SELECT pp.id, p.nama FROM penyaluran_pembekal pp, pembekal p WHERE pp.pembekal_id = p.id");
        $senaraiPembekal = [];
        foreach ($rows as $row) {
            $senaraiPembekal[$row->id] = $row->nama;
        }
        $senaraiSkim = Skim::select('id', 'nama')->get();
        $senaraiPanel = User::select('id', 'nama')->get();
        return view('tuntutan.create', compact('senaraiPembekal', 'senaraiSkim', 'senaraiPanel'));
    }

    public function store(Request $request)
    {
        $tuntutan = new Tuntutan();
        $tuntutan->pp_id = $request->pp_id;
        $tuntutan->jum_tuntutan = $request->jum_tuntutan;
        $tuntutan->tkh_tuntutan = $request->tkh_tuntutan;
        $tuntutan->penyemak_id = $request->penyemak_id;
        $tuntutan->no_rujukan_fail = $request->no_rujukan_fail;
        $tuntutan->status = 1; //Baru
        $tuntutan->save();
        return redirect()->route('tuntutan.index')->with('alert-success', 'Rekod berjaya ditambah');
    }

//Simpan daftar tuntutan
    public function terimaDataPendaftaran(Request $request)
    {
        // Proses validasi dan dapatkan semua data yang di-validate
        $data = $request->validate([
            'no_fail' => ['min:3'],
            'skim_id' => ['min:3'],
            'pembekal_id' => ['min:3'],
            'tahun' => ['integer'],
            'tarikh_tuntutan' => ['min:3'],
            'tarikh_tuntutan_baru' => ['min:3'],
            'jumlah_peruntukan' => ['min:3'],
            'baki_peruntukan' => ['min:3'],
            'kuota' => ['min:3'],

        ]);

        // Beri response redirect ke senarai skim
        return redirect()->route('tuntutan.index')
            ->with('alert-success', 'Rekod berjaya ditambah');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Tuntutan  $tuntutan
     * @return \Illuminate\Http\Response
     */
    public function show(Tuntutan $tuntutan)
    {
        if (auth()->user()->hasAnyRole('Ketua Panel BPB', 'Panel BPB')) {
            return view('tuntutan.detail-panel', compact('tuntutan'));
        }

        return view('tuntutan.detail', compact('tuntutan'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Tuntutan  $tuntutan
     * @return \Illuminate\Http\Response
     */
    public function paparBorangEdit($id)
    {
        $tuntutan = Tuntutan::findOrFail($id);

        $senaraiTuntutan = Tuntutan::select('id')->get();

        return view('tuntutan.borang-edit', compact('tuntutan'));

    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Tuntutan  $tuntutan
     * @return \Illuminate\Http\Response
     */
    // function terimaDataEdit(Request $request, $id)
    // {
    // Proses validasi dan dapatkan semua data yang di-validate
    //  $data = $request->validate([
    //      'no_fail' => ['min:3'],
    //      'skim_id' => ['min:3'],
    //      'pembekal_id' => ['min:3'],
    //      'tahun' => ['integer'],
    //      'tarikh_tuntutan' => ['min:3'],
    //       'tarikh_tuntutan_baru' => ['min:3'],
    //      'jumlah_peruntukan' => ['min:3'],
    //      'baki_peruntukan' => ['min:3'],
    //      'kuota' => ['min:3'],
    //     'dokumen' => ['min:3'],
    //     'catatan' => ['min:3'],
    //     'semakan_panel' => ['min:3'],

    //  ]);

    // Kemaskini data ke dalam table skim
    //  $tuntutan = tuntutan::findOrFail($id);
    //  $tuntutan->update($id);

    // Beri response redirect ke senarai skim
    //   return redirect()->route('tuntutan.index')
    //   ->with('alert-success', 'Rekod berjaya dikemaskini');
    // }

    public function update(Request $request, Tuntutan $tuntutan)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Tuntutan  $tuntutan
     * @return \Illuminate\Http\Response
     */
    public function destroy(Tuntutan $tuntutan)
    {
        //
    }
}
