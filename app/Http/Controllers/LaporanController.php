<?php
namespace App\Http\Controllers;
use Illuminate\Http\Request;

class LaporanController extends Controller
{
    public function tuntutan(Request $req) {
        $filter = '';
        if ($req->has('tkh_dari')) {
            // user submit form to filter data
            $tkh_dari = $req->tkh_dari;
            $tkh_hingga = $req->tkh_hingga;
            $pp_id = 1; // ini patut baca dari form
            $filter = "tkh_tuntutan BETWEEN '$tkh_dari' AND '$tkh_hingga' AND ";

            if (! empty($pp_id))
                $filter = " pp_id = $pp_id AND ";
        }

        $sql1 = "SELECT t.pp_id, pp.kuota, s.nama AS nama_skim, pkl.nama AS nama_pembekal,
        SUM(kuantiti) AS tm, sum(t.jum_tuntutan) AS jum_tuntutan
        FROM tuntutan t, penyaluran_pembekal pp, penyaluran p, skim s, pembekal pkl
        WHERE
        $filter
        t.pp_id = pp.id AND
        pp.penyaluran_id = p.id AND
        p.skim_id = s.id AND
        pp.pembekal_id = pkl.id
        GROUP BY t.pp_id, pp.jum_kontrak, s.nama";

        $rows = \DB::select($sql1);
        foreach($rows as $row) {
            $pp_id = $row->pp_id;
            $sql2 = "SELECT SUM(jum_tuntutan) AS jum_bayar
                     FROM tuntutan
                     WHERE pp_id = $pp_id AND STATUS = 5";
            $rows2 = \DB::select($sql2);
            // kira % kuota yg telah dibekalkan
            $kuota = $row->kuota;
            $mt = $row->tm;
            $pct = $mt / $kuota * 100;
            $row->pct = round($pct, 0);

            $jum_bayar = ($rows2[0])->jum_bayar;
            $row->jum_bayar = $jum_bayar;
        }
        //dd($rows);
        return view('laporan.tuntutan', compact('rows'));
    }
}
