<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Pembekal extends Model
{
    use HasFactory;

    protected $table = 'pembekal';

    protected $fillable = [
        'kod',
        'nama',
        'alamat',
        'telefon',
        'fax',
        'kuota',
        'tahun',
        'pegawai_bertugas',
        'rujukan',
        'status'
    ];
}
