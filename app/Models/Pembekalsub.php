<?php
namespace App\Models;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Pembekalsub extends Model
{
    use HasFactory;
    public $table = 'pembekal_sub';
}
