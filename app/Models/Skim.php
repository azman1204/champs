<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

use Kalnoy\Nestedset\NodeTrait;

class Skim extends Model
{
    use HasFactory;
    use NodeTrait;

    public $table = 'skim';

    protected $fillable = [
        'nama',
        'program_id'
    ];
}
