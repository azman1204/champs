<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Tuntutan extends Model
{
    use HasFactory;

    protected $table = 'tuntutan';

    protected $fillable = [
        'no_fail',
        'skim_id',
        'pembekal_id',
        'tahun',
        'tarikh_tuntutan',
        'jumlah_tuntutan',
        'jumlah_peruntukan',
        'baki_peruntukan',
        'kuota',
    ];

    // Status tuntutan
    public const STATUS_TUNTUTAN_BARU = 'Tuntutan Baru';
    //public const STATUS_SEMAKAN_TUNTUTAN = 'Semakan Tuntutan';
    public const STATUS_PENYEDIAAN_PERAKUAN = 'Semakan Tuntutan';
    public const STATUS_SEMAKAN_SEMULA = 'Semakan Semula';
    public const STATUS_TERIMAAN_SEMULA_TUNTUTAN = 'Terimaan Semula Tuntutan';
    public const STATUS_PERAKUAN_TUNTUTAN = 'Perakuan Tuntutan';
    public const STATUS_SAH_PERAKUAN = 'Sah Perakuan';
    public const STATUS_TIDAK_SAH_PERAKUAN = 'Sah Perakuan';
    public const STATUS_TERIMAAN_SEMULA_PERAKUAN = 'Terimaan Semula Perakuan';
    public const STATUS_TERIMAAN_SEMULA_SAH_PERAKUAN = 'Terimaan Semula Sah Perakuan';
    public const STATUS_SEMAKAN_KEWANGAN = 'Semakan Kewangan';
    public const STATUS_BAYARAN_SELESAI = 'Bayaran Selesai';

    // Tuntutan mempunyai banyak panel dari table tuntutan_panels
    public function panel()
    {
        return $this->hasMany(TuntutanPanel::class);
    }

    // Tuntutan mempunyai banyak panel dari table tuntutan_statuses
    public function status()
    {
        return $this->hasMany(TuntutanStatus::class);
    }

    public function pembekal()
    {
        return $this->belongsTo(Pembekal::class);
    }

    public function skim()
    {
        return $this->belongsTo(Skim::class);
    }
}
