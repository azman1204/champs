SELECT SUM(jum_tuntutan) FROM tuntutan;
SELECT MONTH(tkh_tuntutan), SUM(jum_tuntutan) FROM tuntutan GROUP BY MONTH(tkh_tuntutan);

SELECT * FROM kontrak k, penyaluran p
WHERE p.kontrak_id = k.id;

SELECT * FROM kontrak k, penyaluran p, penyaluran_pembekal pp
WHERE p.kontrak_id = k.id AND pp.penyaluran_id = p.id;

SELECT * FROM kontrak k, penyaluran p, penyaluran_pembekal pp, tuntutan t
WHERE p.kontrak_id = k.id AND pp.penyaluran_id = p.id AND t.id_pp = t.id;

SELECT nama, jum_peruntukan, SUM(jum_tuntutan) 
FROM kontrak k, penyaluran p, penyaluran_pembekal pp, tuntutan t
WHERE p.kontrak_id = k.id AND pp.penyaluran_id = p.id AND t.id_pp = t.id
GROUP BY nama, jum_peruntukan;