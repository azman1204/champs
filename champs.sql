-- --------------------------------------------------------
-- Host:                         127.0.0.1
-- Server version:               5.7.33 - MySQL Community Server (GPL)
-- Server OS:                    Win64
-- HeidiSQL Version:             12.3.0.6589
-- --------------------------------------------------------

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET NAMES utf8 */;
/*!50503 SET NAMES utf8mb4 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;


-- Dumping database structure for stbpdb
CREATE DATABASE IF NOT EXISTS `champs` /*!40100 DEFAULT CHARACTER SET utf8 COLLATE utf8_unicode_ci */;
USE `champs`;

-- Dumping structure for table stbpdb.bahagian
CREATE TABLE IF NOT EXISTS `bahagian` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `nama` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- Dumping data for table stbpdb.bahagian: ~2 rows (approximately)
INSERT INTO `bahagian` (`id`, `nama`, `created_at`, `updated_at`) VALUES
	(1, 'Padi & Beras', NULL, NULL),
	(2, 'Urusetia Ketua Pengarah', NULL, NULL);

-- Dumping structure for table stbpdb.bayaran
CREATE TABLE IF NOT EXISTS `bayaran` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `tuntutan_id` bigint(20) unsigned NOT NULL,
  `skim_id` bigint(20) unsigned NOT NULL,
  `pembekal_id` bigint(20) unsigned NOT NULL,
  `catatan` text COLLATE utf8mb4_unicode_ci,
  `status` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `bayaran_skim_id_foreign` (`skim_id`),
  KEY `bayaran_pembekal_id_foreign` (`pembekal_id`),
  KEY `bayaran_tuntutan_id_foreign` (`tuntutan_id`),
  CONSTRAINT `bayaran_pembekal_id_foreign` FOREIGN KEY (`pembekal_id`) REFERENCES `pembekal` (`id`),
  CONSTRAINT `bayaran_skim_id_foreign` FOREIGN KEY (`skim_id`) REFERENCES `skims` (`id`),
  CONSTRAINT `bayaran_tuntutan_id_foreign` FOREIGN KEY (`tuntutan_id`) REFERENCES `tuntutan` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- Dumping data for table stbpdb.bayaran: ~0 rows (approximately)

-- Dumping structure for table stbpdb.failed_jobs
CREATE TABLE IF NOT EXISTS `failed_jobs` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `uuid` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `connection` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `queue` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `payload` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `exception` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `failed_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`),
  UNIQUE KEY `failed_jobs_uuid_unique` (`uuid`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- Dumping data for table stbpdb.failed_jobs: ~0 rows (approximately)

-- Dumping structure for table stbpdb.jawatan
CREATE TABLE IF NOT EXISTS `jawatan` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `nama` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=14 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- Dumping data for table stbpdb.jawatan: ~13 rows (approximately)
INSERT INTO `jawatan` (`id`, `nama`, `created_at`, `updated_at`) VALUES
	(1, 'Eksekutif Cawangan Pengurusan Dasar ', NULL, NULL),
	(2, 'Penolong Akaun Cawangan Pengurusan Dasar ', NULL, NULL),
	(3, 'Pembantu Ehwal Ekonomi Cawangan Pengurusan Dasar ', NULL, NULL),
	(4, 'Penolong Pengarah Cawangan Pengurusan Dasar ', NULL, NULL),
	(5, 'Ketua Cawangan Pengurusan Dasar ', NULL, NULL),
	(6, 'Eksekutif Cawangan Input Pertanian & Insentif ', NULL, NULL),
	(7, 'Eksekutif Kanan Cawangan Input Pertanian & Insentif ', NULL, NULL),
	(8, 'Penolong Akaun Cawangan Input Pertanian & Insentif ', NULL, NULL),
	(9, 'Pembantu Ehwal Ekonomi  Cawangan Input Pertanian & Insentif ', NULL, NULL),
	(10, 'Penolong Pengarah Cawangan Input Pertanian & Insentif ', NULL, NULL),
	(11, 'Ketua Cawangan Input Pertanian & Insentif ', NULL, NULL),
	(12, 'Pengarah Bahagian Padi & Beras ', NULL, NULL),
	(13, 'Urusetia Ketua Pengarah ', NULL, NULL);

-- Dumping structure for table stbpdb.migrations
CREATE TABLE IF NOT EXISTS `migrations` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `migration` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=16 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- Dumping data for table stbpdb.migrations: ~15 rows (approximately)
INSERT INTO `migrations` (`id`, `migration`, `batch`) VALUES
	(1, '2013_11_14_075243_create_bahagian_table', 1),
	(2, '2013_11_14_075429_create_jawatan_table', 1),
	(3, '2014_10_12_000000_create_users_table', 1),
	(4, '2014_10_12_100000_create_password_resets_table', 1),
	(5, '2019_08_19_000000_create_failed_jobs_table', 1),
	(6, '2019_12_14_000001_create_personal_access_tokens_table', 1),
	(7, '2022_11_14_083759_add_telefon_to_users_table', 1),
	(8, '2022_11_14_085015_create_skims_table', 1),
	(9, '2022_11_15_032648_create_permission_tables', 1),
	(10, '2022_11_16_202944_create_pembekal_table', 1),
	(11, '2022_11_16_203714_create_tuntutan_table', 1),
	(12, '2022_11_16_205533_create_bayaran_table', 1),
	(13, '2022_11_17_040927_create_tuntutan_panels_table', 1),
	(14, '2022_11_17_045150_create_tuntutan_statuses_table', 1),
	(15, '2022_12_16_082021_create_panel_table', 2);

-- Dumping structure for table stbpdb.model_has_permissions
CREATE TABLE IF NOT EXISTS `model_has_permissions` (
  `permission_id` bigint(20) unsigned NOT NULL,
  `model_type` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `model_id` bigint(20) unsigned NOT NULL,
  PRIMARY KEY (`permission_id`,`model_id`,`model_type`),
  KEY `model_has_permissions_model_id_model_type_index` (`model_id`,`model_type`),
  CONSTRAINT `model_has_permissions_permission_id_foreign` FOREIGN KEY (`permission_id`) REFERENCES `permissions` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- Dumping data for table stbpdb.model_has_permissions: ~0 rows (approximately)

-- Dumping structure for table stbpdb.model_has_roles
CREATE TABLE IF NOT EXISTS `model_has_roles` (
  `role_id` bigint(20) unsigned NOT NULL,
  `model_type` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `model_id` bigint(20) unsigned NOT NULL,
  PRIMARY KEY (`role_id`,`model_id`,`model_type`),
  KEY `model_has_roles_model_id_model_type_index` (`model_id`,`model_type`),
  CONSTRAINT `model_has_roles_role_id_foreign` FOREIGN KEY (`role_id`) REFERENCES `roles` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- Dumping data for table stbpdb.model_has_roles: ~12 rows (approximately)
INSERT INTO `model_has_roles` (`role_id`, `model_type`, `model_id`) VALUES
	(3, 'App\\Models\\User', 2),
	(4, 'App\\Models\\User', 3),
	(2, 'App\\Models\\User', 4),
	(3, 'App\\Models\\User', 4),
	(2, 'App\\Models\\User', 5),
	(3, 'App\\Models\\User', 6),
	(7, 'App\\Models\\User', 7),
	(2, 'App\\Models\\User', 8),
	(2, 'App\\Models\\User', 9),
	(1, 'App\\Models\\User', 10),
	(2, 'App\\Models\\User', 11),
	(3, 'App\\Models\\User', 11),
	(4, 'App\\Models\\User', 12);

-- Dumping structure for table stbpdb.password_resets
CREATE TABLE IF NOT EXISTS `password_resets` (
  `email` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `token` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  KEY `password_resets_email_index` (`email`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- Dumping data for table stbpdb.password_resets: ~0 rows (approximately)

-- Dumping structure for table stbpdb.pembekal
CREATE TABLE IF NOT EXISTS `pembekal` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `kod` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `nama` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `alamat` text COLLATE utf8mb4_unicode_ci,
  `telefon` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `fax` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `kuota` decimal(16,2) NOT NULL DEFAULT '0.00',
  `tahun` int(11) NOT NULL,
  `pegawai_bertugas` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `status` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `rujukan` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=12 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- Dumping data for table stbpdb.pembekal: ~8 rows (approximately)
INSERT INTO `pembekal` (`id`, `kod`, `nama`, `alamat`, `telefon`, `fax`, `kuota`, `tahun`, `pegawai_bertugas`, `status`, `rujukan`, `created_at`, `updated_at`) VALUES
	(1, 'ABC123', 'Syarikat A', 'Alamat Syarikat A', '0123456789', '0326970953', 0.00, 2022, 'Ahmad', 'AKTIF', 'ABC123', '2022-11-21 00:03:28', '2022-11-24 20:45:45'),
	(2, 'XYZ123', 'Syarikat B', 'Alamat Syarikat B', '0123456789', '0326970955', 0.00, 2022, 'Ali', 'AKTIF', 'XYZ123', '2022-11-21 00:03:28', '2022-11-24 20:45:52'),
	(3, '1000/002', 'NAFAS', 'SERI GOMBAK', '0145392855', '0326970953', 0.00, 2022, 'Farhain', 'TAK AKTIF', '1000/002/NAFAS', '2022-11-23 19:30:39', '2022-11-24 20:45:57'),
	(4, '1000/003', 'PPK GOMBAK', 'B-4 Kampung Batu Putih', '0353333334', '0326970953', 0.00, 2022, 'Massita Shohari', 'TAK AKTIF', '1000/003/NAFAS', '2022-11-23 20:01:24', '2022-11-24 20:46:18'),
	(8, '1000/007', 'Zaiton', 'Batu Caves', '0353333333', '0326970953', 0.00, 2022, 'Zaiton', 'AKTIF', '2022/007/NAFAS', '2022-11-24 18:38:21', '2022-11-24 20:46:24'),
	(9, '1000/008', 'Syarikat C', 'Sentul Baru', '0326970952', '0326970955', 0.00, 2022, 'Junaidah Ahmad Darwin', 'AKTIF', '2022/008/NAFAS', '2022-11-24 18:49:01', '2022-11-24 20:50:09'),
	(10, '1000/008', 'NAFAS', 'PPK Kuantan', '0326970987', '0326970953', 0.00, 2022, 'Abu Tausi', 'AKTIF', '2022/008/NAFAS', '2022-11-24 19:31:44', '2022-11-24 20:50:17'),
	(11, '1000/008', 'NAFAS/2022', 'Sentul Baru', '0353333334', '0326970955', 0.00, 2022, 'Semek', 'AKTIF', '2022/008/NAFAS', '2022-11-24 20:53:12', '2022-11-28 21:54:31');

-- Dumping structure for table stbpdb.permissions
CREATE TABLE IF NOT EXISTS `permissions` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `guard_name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `permissions_name_guard_name_unique` (`name`,`guard_name`)
) ENGINE=InnoDB AUTO_INCREMENT=34 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- Dumping data for table stbpdb.permissions: ~33 rows (approximately)
INSERT INTO `permissions` (`id`, `name`, `guard_name`, `created_at`, `updated_at`) VALUES
	(1, 'superadmin.all.areas', 'web', '2022-11-21 00:03:28', '2022-11-21 00:03:28'),
	(2, 'tuntutan.view', 'web', '2022-11-21 00:03:28', '2022-11-21 00:03:28'),
	(3, 'tuntutan.create', 'web', '2022-11-21 00:03:28', '2022-11-21 00:03:28'),
	(4, 'tuntutan.edit', 'web', '2022-11-21 00:03:28', '2022-11-21 00:03:28'),
	(5, 'tuntutan.delete', 'web', '2022-11-21 00:03:28', '2022-11-21 00:03:28'),
	(6, 'users.view', 'web', '2022-11-21 00:03:28', '2022-11-21 00:03:28'),
	(7, 'users.create', 'web', '2022-11-21 00:03:28', '2022-11-21 00:03:28'),
	(8, 'users.edit', 'web', '2022-11-21 00:03:28', '2022-11-21 00:03:28'),
	(9, 'users.delete', 'web', '2022-11-21 00:03:28', '2022-11-21 00:03:28'),
	(10, 'pembekal.view', 'web', '2022-11-21 00:03:28', '2022-11-21 00:03:28'),
	(11, 'pembekal.create', 'web', '2022-11-21 00:03:28', '2022-11-21 00:03:28'),
	(12, 'pembekal.edit', 'web', '2022-11-21 00:03:28', '2022-11-21 00:03:28'),
	(13, 'pembekal.delete', 'web', '2022-11-21 00:03:28', '2022-11-21 00:03:28'),
	(14, 'skim.view', 'web', '2022-11-21 00:03:28', '2022-11-21 00:03:28'),
	(15, 'skim.create', 'web', '2022-11-21 00:03:28', '2022-11-21 00:03:28'),
	(16, 'skim.edit', 'web', '2022-11-21 00:03:28', '2022-11-21 00:03:28'),
	(17, 'skim.delete', 'web', '2022-11-21 00:03:28', '2022-11-21 00:03:28'),
	(18, 'jawatan.view', 'web', '2022-11-21 00:03:28', '2022-11-21 00:03:28'),
	(19, 'jawatan.create', 'web', '2022-11-21 00:03:28', '2022-11-21 00:03:28'),
	(20, 'jawatan.edit', 'web', '2022-11-21 00:03:28', '2022-11-21 00:03:28'),
	(21, 'jawatan.delete', 'web', '2022-11-21 00:03:28', '2022-11-21 00:03:28'),
	(22, 'bahagian.view', 'web', '2022-11-21 00:03:28', '2022-11-21 00:03:28'),
	(23, 'bahagian.create', 'web', '2022-11-21 00:03:28', '2022-11-21 00:03:28'),
	(24, 'bahagian.edit', 'web', '2022-11-21 00:03:28', '2022-11-21 00:03:28'),
	(25, 'bahagian.delete', 'web', '2022-11-21 00:03:28', '2022-11-21 00:03:28'),
	(26, 'bayaran.view', 'web', '2022-11-21 00:03:28', '2022-11-21 00:03:28'),
	(27, 'bayaran.create', 'web', '2022-11-21 00:03:28', '2022-11-21 00:03:28'),
	(28, 'bayaran.edit', 'web', '2022-11-21 00:03:28', '2022-11-21 00:03:28'),
	(29, 'bayaran.delete', 'web', '2022-11-21 00:03:28', '2022-11-21 00:03:28'),
	(30, 'peranan.view', 'web', '2022-11-21 00:03:28', '2022-11-21 00:03:28'),
	(31, 'peranan.create', 'web', '2022-11-21 00:03:28', '2022-11-21 00:03:28'),
	(32, 'peranan.edit', 'web', '2022-11-21 00:03:28', '2022-11-21 00:03:28'),
	(33, 'peranan.delete', 'web', '2022-11-21 00:03:28', '2022-11-21 00:03:28');

-- Dumping structure for table stbpdb.personal_access_tokens
CREATE TABLE IF NOT EXISTS `personal_access_tokens` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `tokenable_type` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `tokenable_id` bigint(20) unsigned NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `token` varchar(64) COLLATE utf8mb4_unicode_ci NOT NULL,
  `abilities` text COLLATE utf8mb4_unicode_ci,
  `last_used_at` timestamp NULL DEFAULT NULL,
  `expires_at` timestamp NULL DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `personal_access_tokens_token_unique` (`token`),
  KEY `personal_access_tokens_tokenable_type_tokenable_id_index` (`tokenable_type`,`tokenable_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- Dumping data for table stbpdb.personal_access_tokens: ~0 rows (approximately)

-- Dumping structure for table stbpdb.roles
CREATE TABLE IF NOT EXISTS `roles` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `guard_name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `roles_name_guard_name_unique` (`name`,`guard_name`)
) ENGINE=InnoDB AUTO_INCREMENT=12 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- Dumping data for table stbpdb.roles: ~11 rows (approximately)
INSERT INTO `roles` (`id`, `name`, `guard_name`, `created_at`, `updated_at`) VALUES
	(1, 'Super Admin', 'web', '2022-11-21 00:03:28', '2022-11-21 00:03:28'),
	(2, 'Admin BPB', 'web', '2022-11-21 00:03:28', '2022-11-21 00:03:28'),
	(3, 'Ketua Panel BPB', 'web', '2022-11-21 00:03:28', '2022-11-21 00:03:28'),
	(4, 'Panel BPB', 'web', '2022-11-21 00:03:28', '2022-11-21 00:03:28'),
	(5, 'Pendaftar Progam/Skim', 'web', '2022-11-21 00:03:28', '2022-11-21 00:03:28'),
	(6, 'Pengesahan Daftar Progam/Skim', 'web', '2022-11-21 00:03:28', '2022-11-21 00:03:28'),
	(7, 'Pendaftar Pembekal', 'web', '2022-11-21 00:03:28', '2022-11-21 00:03:28'),
	(8, 'Pengesahan Daftar Pembekal', 'web', '2022-11-21 00:03:28', '2022-11-21 00:03:28'),
	(9, 'Peraku Bayaran', 'web', '2022-11-21 00:03:28', '2022-11-21 00:03:28'),
	(10, 'Pengesahan Peraku Bayaran', 'web', '2022-11-21 00:03:28', '2022-11-21 00:03:28'),
	(11, 'Urusetia Ketua Pengarah', 'web', '2022-11-21 00:03:28', '2022-11-21 00:03:28');

-- Dumping structure for table stbpdb.role_has_permissions
CREATE TABLE IF NOT EXISTS `role_has_permissions` (
  `permission_id` bigint(20) unsigned NOT NULL,
  `role_id` bigint(20) unsigned NOT NULL,
  PRIMARY KEY (`permission_id`,`role_id`),
  KEY `role_has_permissions_role_id_foreign` (`role_id`),
  CONSTRAINT `role_has_permissions_permission_id_foreign` FOREIGN KEY (`permission_id`) REFERENCES `permissions` (`id`) ON DELETE CASCADE,
  CONSTRAINT `role_has_permissions_role_id_foreign` FOREIGN KEY (`role_id`) REFERENCES `roles` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- Dumping data for table stbpdb.role_has_permissions: ~29 rows (approximately)
INSERT INTO `role_has_permissions` (`permission_id`, `role_id`) VALUES
	(1, 1),
	(2, 3),
	(3, 3),
	(4, 3),
	(5, 3),
	(6, 3),
	(7, 3),
	(8, 3),
	(9, 3),
	(10, 3),
	(11, 3),
	(12, 3),
	(13, 3),
	(14, 3),
	(15, 3),
	(16, 3),
	(17, 3),
	(18, 3),
	(19, 3),
	(20, 3),
	(21, 3),
	(22, 3),
	(23, 3),
	(24, 3),
	(25, 3),
	(26, 3),
	(27, 3),
	(28, 3),
	(29, 3);

-- Dumping structure for table stbpdb.skims
CREATE TABLE IF NOT EXISTS `skims` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `_lft` int(10) unsigned NOT NULL DEFAULT '0',
  `_rgt` int(10) unsigned NOT NULL DEFAULT '0',
  `parent_id` int(10) unsigned DEFAULT NULL,
  `nama` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `vot` varchar(50) COLLATE utf8mb4_unicode_ci NOT NULL,
  `tahun` int(10) unsigned NOT NULL,
  `jumlah_peruntukan` decimal(16,2) NOT NULL DEFAULT '0.00',
  `baki_peruntukan` decimal(16,2) NOT NULL DEFAULT '0.00',
  `tarikh_kelulusan` date DEFAULT NULL,
  `rujukan_kelulusan` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `virement` decimal(20,2) DEFAULT '0.00',
  `pelarasan` decimal(20,2) DEFAULT '0.00',
  `belanja` decimal(20,2) DEFAULT '0.00',
  `catatan` text COLLATE utf8mb4_unicode_ci,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `skims__lft__rgt_parent_id_index` (`_lft`,`_rgt`,`parent_id`)
) ENGINE=InnoDB AUTO_INCREMENT=9 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- Dumping data for table stbpdb.skims: ~8 rows (approximately)
INSERT INTO `skims` (`id`, `_lft`, `_rgt`, `parent_id`, `nama`, `vot`, `tahun`, `jumlah_peruntukan`, `baki_peruntukan`, `tarikh_kelulusan`, `rujukan_kelulusan`, `virement`, `pelarasan`, `belanja`, `catatan`, `created_at`, `updated_at`) VALUES
	(1, 1, 2, NULL, 'Skim Bantuan Padi', '1-000-500-96001', 2022, 10000000.00, 10000000.00, '2022-10-01', NULL, 0.00, 0.00, 0.00, NULL, '2022-11-21 00:03:28', '2022-11-21 00:03:28'),
	(2, 3, 4, NULL, 'Bantuan Pesawah', '1-000-500-96002', 2022, 20000000.00, 20000000.00, '2022-10-01', NULL, 0.00, 0.00, 0.00, NULL, '2022-11-21 00:03:28', '2022-12-01 21:45:06'),
	(3, 5, 6, NULL, 'Tabung Bencana Tanaman Padi', '1-000-500-96003', 2022, 80000000.00, 80000000.00, '2022-10-01', 'nafas/1022', 0.00, 0.00, 0.00, 'peruntukan tahun 2021', '2022-11-21 00:03:28', '2022-12-01 21:36:27'),
	(4, 7, 8, NULL, 'Bantuan Pesawah', '1-000-1500-69019', 2022, 10494864.00, 10494864.00, '2022-02-07', 'nafas/1000', 0.00, 0.00, 0.00, 'peruntukan tahun 2021', '2022-12-01 18:56:35', '2022-12-01 18:56:35'),
	(5, 9, 10, NULL, 'SIPP-Kapur tahun 2022', '1000150096037', 2022, 1111111.00, 1111111.00, '2022-12-12', 'nafas/115', 0.00, 0.00, 0.00, 'peruntukan tahun 2021', '2022-12-01 22:27:53', '2022-12-01 23:01:12'),
	(6, 11, 12, NULL, 'SIPP-Kapur tahun 2022', '1000150096037', 2022, 400000.00, 400000.00, '2022-12-12', 'nafas/126', 0.00, 0.00, 0.00, 'peruntukan tahun 2021', '2022-12-01 22:43:23', '2022-12-01 23:15:16'),
	(7, 13, 14, NULL, 'SIPP-Pembajakan tahun 2022', '1-000-1500-69048', 2020, 8551223.17, 8551223.17, '2022-12-12', 'nafas/048', 0.00, 0.00, 0.00, 'Peruntukan tahun 2020', '2022-12-01 23:17:23', '2022-12-02 00:01:49'),
	(8, 15, 16, NULL, 'SIPP-Baja Tambahan NPK 2021', '1-000-1500-96057', 2021, 50212169.38, 50212169.38, '2021-01-04', 'Nafas/009', 0.00, 0.00, 0.00, 'Peruntukan tahun 2021', '2022-12-02 00:05:26', '2022-12-02 00:05:26');

-- Dumping structure for table stbpdb.tuntutan
CREATE TABLE IF NOT EXISTS `tuntutan` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `no_fail` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `skim_id` bigint(20) unsigned NOT NULL,
  `pembekal_id` bigint(20) unsigned NOT NULL,
  `tahun` int(11) NOT NULL,
  `tarikh_tuntutan` date DEFAULT NULL,
  `jumlah_tuntutan` decimal(16,2) NOT NULL DEFAULT '0.00',
  `jumlah_peruntukan` decimal(16,2) NOT NULL DEFAULT '0.00',
  `baki_peruntukan` decimal(16,2) NOT NULL DEFAULT '0.00',
  `kuota` decimal(16,2) NOT NULL DEFAULT '0.00',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `tuntutan_skim_id_foreign` (`skim_id`),
  KEY `tuntutan_pembekal_id_foreign` (`pembekal_id`),
  CONSTRAINT `tuntutan_pembekal_id_foreign` FOREIGN KEY (`pembekal_id`) REFERENCES `pembekal` (`id`),
  CONSTRAINT `tuntutan_skim_id_foreign` FOREIGN KEY (`skim_id`) REFERENCES `skims` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- Dumping data for table stbpdb.tuntutan: ~3 rows (approximately)
INSERT INTO `tuntutan` (`id`, `no_fail`, `skim_id`, `pembekal_id`, `tahun`, `tarikh_tuntutan`, `jumlah_tuntutan`, `jumlah_peruntukan`, `baki_peruntukan`, `kuota`, `created_at`, `updated_at`) VALUES
	(1, '123456/abcd', 2, 2, 2022, '2022-11-23', 500000.00, 1000000.00, 1000000.00, 1000000.00, '2022-11-22 17:02:23', '2022-11-22 17:02:23'),
	(2, 'nafas/1234', 2, 3, 2022, '2022-12-01', 200000.00, 1000000.00, 1000000.00, 1000000.00, '2022-11-30 20:42:29', '2022-11-30 20:42:29'),
	(3, 'nafas/551/20', 6, 3, 2022, '2022-12-12', 500000.00, 1000000.00, 1000000.00, 1000000.00, '2022-12-13 16:10:49', '2022-12-13 16:10:49');

-- Dumping structure for table stbpdb.tuntutan_panels
CREATE TABLE IF NOT EXISTS `tuntutan_panels` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `tuntutan_id` bigint(20) unsigned NOT NULL,
  `user_id` bigint(20) unsigned NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `tuntutan_panels_tuntutan_id_foreign` (`tuntutan_id`),
  KEY `tuntutan_panels_user_id_foreign` (`user_id`),
  CONSTRAINT `tuntutan_panels_tuntutan_id_foreign` FOREIGN KEY (`tuntutan_id`) REFERENCES `tuntutan` (`id`),
  CONSTRAINT `tuntutan_panels_user_id_foreign` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- Dumping data for table stbpdb.tuntutan_panels: ~3 rows (approximately)
INSERT INTO `tuntutan_panels` (`id`, `tuntutan_id`, `user_id`, `created_at`, `updated_at`) VALUES
	(1, 1, 1, '2022-11-22 17:02:23', '2022-11-22 17:02:23'),
	(2, 2, 2, '2022-11-30 20:42:29', '2022-11-30 20:42:29'),
	(3, 3, 1, '2022-12-13 16:10:49', '2022-12-13 16:10:49');

-- Dumping structure for table stbpdb.tuntutan_statuses
CREATE TABLE IF NOT EXISTS `tuntutan_statuses` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `tuntutan_id` bigint(20) unsigned NOT NULL,
  `status` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `users_nama` varchar(50) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `tuntutan_statuses_tuntutan_id_foreign` (`tuntutan_id`),
  CONSTRAINT `tuntutan_statuses_tuntutan_id_foreign` FOREIGN KEY (`tuntutan_id`) REFERENCES `tuntutan` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- Dumping data for table stbpdb.tuntutan_statuses: ~3 rows (approximately)
INSERT INTO `tuntutan_statuses` (`id`, `tuntutan_id`, `status`, `users_nama`, `created_at`, `updated_at`) VALUES
	(1, 1, 'Tuntutan Baru', '', '2022-11-22 17:02:23', '2022-11-22 17:02:23'),
	(2, 2, 'Tuntutan Baru', '', '2022-11-30 20:42:29', '2022-11-30 20:42:29'),
	(3, 3, 'Tuntutan Baru', '', '2022-12-13 16:10:49', '2022-12-13 16:10:49');

-- Dumping structure for table stbpdb.users
CREATE TABLE IF NOT EXISTS `users` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `no_kp` varchar(12) COLLATE utf8mb4_unicode_ci NOT NULL,
  `password` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `nama` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `jawatan_id` bigint(20) unsigned NOT NULL,
  `bahagian_id` bigint(20) unsigned NOT NULL,
  `email` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `telefon` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `email_verified_at` timestamp NULL DEFAULT NULL,
  `status` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `remember_token` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `users_no_kp_unique` (`no_kp`),
  UNIQUE KEY `users_email_unique` (`email`),
  KEY `users_jawatan_id_foreign` (`jawatan_id`),
  KEY `users_bahagian_id_foreign` (`bahagian_id`),
  CONSTRAINT `users_bahagian_id_foreign` FOREIGN KEY (`bahagian_id`) REFERENCES `bahagian` (`id`),
  CONSTRAINT `users_jawatan_id_foreign` FOREIGN KEY (`jawatan_id`) REFERENCES `jawatan` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=13 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- Dumping data for table stbpdb.users: ~10 rows (approximately)
INSERT INTO `users` (`id`, `no_kp`, `password`, `nama`, `jawatan_id`, `bahagian_id`, `email`, `telefon`, `email_verified_at`, `status`, `remember_token`, `created_at`, `updated_at`) VALUES
	(1, '100000000001', '$2y$10$Q.IIwgZz6ST1YsWKcauFOuPP7r4ft3eoJaq2xwEgxmJ/SkMlaY/eS', 'Ali', 1, 1, 'ali@gmail.com', '0145392855', NULL, 'AKTIF', NULL, '2022-11-21 00:03:28', '2023-01-04 19:00:42'),
	(2, '100000000002', '$2y$10$UxIH1ULExcbHLzR76Z.Uo.4KKgoxC47gMJWIxQ/QB0ww5mxBB8WhW', 'Abu', 1, 2, 'abu@gmail.com', '0353333334', NULL, 'AKTIF', NULL, '2022-11-21 00:03:28', '2022-11-24 23:49:23'),
	(3, '100000000003', '$2y$10$.YrKBca2hsNHoSmBrCQD7e5Jjbb8zwZpryE0V6VweqBbMmQTM24Ni', 'Siti', 1, 2, 'siti@gmail.com', '0145392855', NULL, 'AKTIF', NULL, '2022-11-21 00:03:28', '2022-11-24 23:48:51'),
	(4, '860319336092', '$2y$10$nfczHSgSST2GGr6HESFgtOI4K1IUqCBovBfQaAoqkM9ydz6Wolx/C', 'zuraidah', 4, 1, 'zuraidahmustapa@gmail.com', '0145392855', NULL, 'AKTIF', NULL, '2022-11-21 00:06:44', '2022-12-22 00:01:44'),
	(6, '100000000004', '$2y$10$aE4//Et8xJqxo21DqojUV.MlLPlYHO7s6A.2Odj5iRo2RMDVg1Vum', 'aziah', 4, 1, 'jun@lpp.gov.my', '0353333333', NULL, 'AKTIF', NULL, '2022-11-24 00:01:30', '2022-11-24 23:48:41'),
	(7, '860319336091', '$2y$10$Gv98M2gJlKGP.ThBN7147eyIb2sHksY..WeGHNqTejwcaHaecfe/i', 'Junaidah', 6, 1, 'junaidah@lpp.gov.my', '0145392855', NULL, 'AKTIF', NULL, '2022-11-24 00:13:17', '2022-11-24 23:48:36'),
	(8, '100000000005', '$2y$10$LKdWQn5sBy51A9C8C3YWpO8qDHfqyOglr9QXYwog3lzy1tU8wzpsq', 'abi', 1, 1, 'abi@gmail.com', '0353333333', NULL, 'AKTIF', NULL, '2022-11-24 17:48:58', '2022-11-24 23:48:30'),
	(9, '860319336093', '$2y$10$p/W6gOqz21Lf/.opaxmiEurB54Zz6O2DTTpyHzIxm3N4yZueUxY.e', 'mus', 1, 1, 'mus@gmail.com', '0353333335', NULL, 'AKTIF', NULL, '2022-11-24 21:32:40', '2022-12-02 00:21:40'),
	(10, '860319336095', '$2y$10$Q.IIwgZz6ST1YsWKcauFOuPP7r4ft3eoJaq2xwEgxmJ/SkMlaY/eS', 'zakiah', 1, 1, 'zakiah@gmail.com', '0145392855', NULL, 'AKTIF', NULL, '2022-11-28 21:18:32', '2022-11-28 21:18:32'),
	(11, '860319336098', '$2y$10$xsNuyxsviEMxZf12oJIwI.ql535yHSDQuFn35H8gvPoOQuRni45mK', 'Mass', 4, 1, 'mass@gmail.com', '0145392855', NULL, 'AKTIF', NULL, '2022-12-22 18:34:52', '2022-12-22 18:34:52'),
	(12, '860319336000', '$2y$10$NHzZrBt2WxHjK6nxM2EcNeGF0HsNS7eEaSB9Cfhx0KqdAeIxVHpC.', 'niza', 9, 1, 'niza@gmail.com', '0145362855', NULL, 'AKTIF', NULL, '2023-01-30 17:16:04', '2023-01-30 17:16:04');

/*!40103 SET TIME_ZONE=IFNULL(@OLD_TIME_ZONE, 'system') */;
/*!40101 SET SQL_MODE=IFNULL(@OLD_SQL_MODE, '') */;
/*!40014 SET FOREIGN_KEY_CHECKS=IFNULL(@OLD_FOREIGN_KEY_CHECKS, 1) */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40111 SET SQL_NOTES=IFNULL(@OLD_SQL_NOTES, 1) */;
